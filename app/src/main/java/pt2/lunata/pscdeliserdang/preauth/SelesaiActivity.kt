package pt2.lunata.pscdeliserdang.preauth

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_selesai.*
import kotlinx.android.synthetic.main.register_step.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.startActivity
import pt2.lunata.pscdeliserdang.App
import pt2.lunata.pscdeliserdang.BaseActivity
import pt2.lunata.pscdeliserdang.R

class SelesaiActivity : BaseActivity() {
    companion object {
        const val CURRENT_BROADCAST = App.BC_SELESAI
    }

    val dataRegisterSharedPref = DataRegisterSharedPref()
    val TAG = this::class.java.simpleName
    val compositeDisposable = CompositeDisposable()

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.action
            if (action.equals(CURRENT_BROADCAST)) {
                finish()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
        compositeDisposable.dispose()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selesai)

        registerReceiver(broadcastReceiver, IntentFilter(CURRENT_BROADCAST))

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.title = ""
        appBar.outlineProvider = null
        toolbar.backgroundColor = pt2.lunata.pscdeliserdang.getColor(
            this,
            R.color.pscPrimary
        )

        registerStepView.statusView.currentCount = 3
        btnLanjutLogin.setText("Lanjut Login")
        btnLanjutLogin.setOnClickListener {
            startActivity<LoginActivity>()
            finish()
        }
        /*Handler().postDelayed({
            finish()
        }, 3000)*/
        /*Handler().postDelayed({
            val loginDisposable = AuthService.create()
                    .login(dataRegisterSharedPref.noHp!!, dataRegisterSharedPref.pass!!, App.DEVICE_TOKEN)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { result ->
                                val data = result.data
                                userPref.uid = data.uid
                                userPref.nama = data.nama
                                userPref.ktp = data.ktp
                                userPref.uidKelamin = data.uidKelamin
                                userPref.hp = data.hp
                                userPref.tglLahir = data.tglLahir
                                userPref.fotoUrl = data.foto
                                userPref.idStatus = data.idStatus

                                dataRegisterSharedPref.clear()

                                startActivity<DashboardActivity>()
                                finish()
                            },
                            {
                                toast("Mohon maaf, ada masalah saat login ulang")
                                finish()
                            }
                    )

            compositeDisposable.add(loginDisposable)
        }, this.resources.getInteger(R.integer.slide_animation_duration).toLong() + 3000)*/
    }

    override fun onBackPressed() {
    }
}
