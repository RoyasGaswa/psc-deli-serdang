package pt2.lunata.pscdeliserdang.component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.google.android.material.chip.Chip
import khronos.Dates.today
import khronos.toDate
import org.jetbrains.anko.find
import pt2.lunata.pscdeliserdang.R

class StatusChips(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

    val labelStatusDitolak: Chip
    val labelStatusBelumDisetujui: Chip
    val labelStatusSudahDisetujui: Chip
    val labelStatusDisetujuiPeralatan: Chip
    val labelStatusPending: Chip
    val labelStatusBerjalan: Chip
    val labelStatusSelesai: Chip
    val labelStatusExpired: Chip

    init {
        inflate(context, R.layout.component_status_chips, this)

        labelStatusDitolak = find(R.id.labelStatusDitolak)
        labelStatusBelumDisetujui = find(R.id.labelStatusBelumDisetujui)
        labelStatusSudahDisetujui = find(R.id.labelStatusSudahDisetujui)
        labelStatusDisetujuiPeralatan = find(R.id.labelStatusDisetujuiPeralatan)
        labelStatusPending = find(R.id.labelStatusPending)
        labelStatusBerjalan = find(R.id.labelStatusBerjalan)
        labelStatusSelesai = find(R.id.labelStatusSelesai)
        labelStatusExpired = find(R.id.labelStatusExpired)
    }

    fun hideAll() {
        labelStatusDitolak.visibility = View.GONE
        labelStatusBelumDisetujui.visibility = View.GONE
        labelStatusSudahDisetujui.visibility = View.GONE
        labelStatusDisetujuiPeralatan.visibility = View.GONE
        labelStatusPending.visibility = View.GONE
        labelStatusBerjalan.visibility = View.GONE
        labelStatusSelesai.visibility = View.GONE
        labelStatusExpired.visibility = View.GONE
    }

    fun setStatusRapat(statusRapat: String, jamSelesai: String, tanggalSelesai: String) {
        val masihDalamDurasi = ("$jamSelesai, $tanggalSelesai").toDate("HH:mm, yyyy-MM-dd") > today
        when {
            statusRapat == "N" && masihDalamDurasi -> belumDisetujuiAtasan()
            statusRapat == "A" -> disetujuiPeralatan()
            statusRapat == "B" -> berjalan()
            statusRapat == "Y" -> disetujuiAtasan()
            statusRapat == "S" -> selesai()
            statusRapat == "X" -> ditolak()
            else -> expired()
        }
    }

    fun ditolak() {
        hideAll()
        labelStatusDitolak.visibility = View.VISIBLE
    }

    fun disetujuiAtasan() {
        hideAll()
        labelStatusSudahDisetujui.visibility = View.VISIBLE
    }

    fun belumDisetujuiAtasan() {
        hideAll()
        labelStatusBelumDisetujui.visibility = View.VISIBLE
    }

    fun disetujuiPeralatan() {
        hideAll()
        labelStatusDisetujuiPeralatan.visibility = View.VISIBLE
    }

    fun pending() {
        hideAll()
        labelStatusPending.visibility = View.VISIBLE
    }

    fun berjalan() {
        hideAll()
        labelStatusBerjalan.visibility = View.VISIBLE
    }

    fun selesai() {
        hideAll()
        labelStatusSelesai.visibility = View.VISIBLE
    }

    fun expired() {
        hideAll()
        labelStatusExpired.visibility = View.VISIBLE
    }

}