package pt2.lunata.pscdeliserdang.preauth

import android.annotation.SuppressLint
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import com.rilixtech.materialfancybutton.MaterialFancyButton
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_foto_ktp.*
import kotlinx.android.synthetic.main.register_step.*
import kotlinx.android.synthetic.main.toolbar.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.jetbrains.anko.*
import pt2.lunata.pscdeliserdang.*
import pt2.lunata.pscdeliserdang.api.AuthService
import java.io.File
import java.io.FileOutputStream
import java.util.*

class FotoKtpActivity : BaseActivity() {
    companion object {
        const val RC_CAMERA_USER = 10
        const val RC_CAMERA_KTP = 11
        const val CURRENT_BROADCAST = App.BC_FOTOKTP
    }

    val dataRegisterSharedPref = DataRegisterSharedPref()
    val TAG = this::class.java.simpleName
    val compositeDisposable = CompositeDisposable()

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.action
            if (action.equals(CURRENT_BROADCAST)) {
                finish()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
        compositeDisposable.dispose()
    }

    @SuppressLint("InflateParams")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_foto_ktp)

        registerReceiver(broadcastReceiver, IntentFilter(CURRENT_BROADCAST))

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.title = ""
        appBar.outlineProvider = null
        toolbar.backgroundColor = pt2.lunata.pscdeliserdang.getColor(
            this,
            R.color.pscPrimary
        )

        registerStepView.statusView.currentCount = 2

        val dialogInfoBuilder = alert {}
        val dialogView = layoutInflater.inflate(R.layout.register_info_foto_ktp_dialog, null)
        val btnOk = dialogView.findViewById<MaterialFancyButton>(R.id.btnOk)

        dialogInfoBuilder.customView = dialogView

        Handler().postDelayed({
            val dialogInfo = dialogInfoBuilder.show()

            btnOk.setOnClickListener { _ ->
                dialogInfo.dismiss()
            }
        }, this.resources.getInteger(R.integer.slide_animation_duration).toLong())

        btnFotoKtp.setOnClickListener {
            val cameraIntent = Intent(this, CameraActivity::class.java)
            cameraIntent.putExtra("confirmation", false)
            cameraIntent.putExtra("gallery-picker", false)
            startActivityForResult(cameraIntent, RC_CAMERA_KTP)
        }

        btnFotoUser.setOnClickListener {
            val cameraIntent = Intent(this, CameraActivity::class.java)
            cameraIntent.putExtra("confirmation", false)
            cameraIntent.putExtra("gallery-picker", false)
            cameraIntent.putExtra("front-camera", true)
            startActivityForResult(cameraIntent, RC_CAMERA_USER)
        }

        btnRegisterLanjut.setOnClickListener {
            btnRegisterLanjut.isEnabled = false

            val alertUpload = indeterminateProgressDialog("Sedang mengirim data") {
                setCancelable(false)
            }
            alertUpload.show()
            uploadFoto(dataRegisterSharedPref.uid!!)
            /*val nama = dataRegisterSharedPref.nama!!
            val email = dataRegisterSharedPref.email!!
            val pass = dataRegisterSharedPref.pass!!
            val bpjs = dataRegisterSharedPref.bpjs!!
            val ktp = dataRegisterSharedPref.ktp!!
            val tglLahir = dataRegisterSharedPref.tglLahir!!
            val kelamin = dataRegisterSharedPref.kelamin!!
            val goldar = dataRegisterSharedPref.goldar!!
            val noHp = dataRegisterSharedPref.noHp!!
            val alamat = dataRegisterSharedPref.alamat!!
            val idKecamatan = dataRegisterSharedPref.idKecamatan!!
            val idKelurahan = dataRegisterSharedPref.idKelurahan!!

            val registerDisposable =
                    AuthService.create()
                            .register(
                                    nama,
                                    email,
                                    pass,
                                    bpjs,
                                    ktp,
                                    tglLahir,
                                    kelamin,
                                    goldar,
                                    noHp,
                                    alamat,
                                    idKecamatan,
                                    idKelurahan
                            )
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    { result ->
                                        val uidUser = result.uid
                                        dataRegisterSharedPref.uid = uidUser
                                        uploadFoto(uidUser)
                                        alertUpload.hide()
                                    },
                                    { t ->
                                        btnRegisterLanjut.isEnabled = true
                                        alertUpload.hide()
                                        Log.e(TAG, t.message)
                                        longToast("Mohon cek koneksi anda")
                                    }
                            )
            compositeDisposable.add(registerDisposable)*/
        }
    }

    private fun uploadFoto(uidUser: String) {
        val alertUpload = indeterminateProgressDialog("Sedang mengunggah Foto") {
            setCancelable(false)
        }
        alertUpload.show()

        val fotoKtpPath = dataRegisterSharedPref.fotoKtpPath
        val ktpFotoBase64 = getFotoToBase64(fotoKtpPath)

        val fotoUserPath = dataRegisterSharedPref.fotoUserPath
        val userFotoBase64 = getFotoToBase64(fotoUserPath)

        val file =File(fotoKtpPath)
        val imageSelfieKtp=RequestBody.create("image/*".toMediaTypeOrNull(), file)
        val filePart=MultipartBody.Part.createFormData("selfie-ktp",file.name,imageSelfieKtp)

        val file2 =File(fotoUserPath)
        val imageSelfie=RequestBody.create("image/*".toMediaTypeOrNull(), file2)
        val filePart2=MultipartBody.Part.createFormData("selfie",file2.name,imageSelfie)

        val stringMediaType = "text/plain".toMediaTypeOrNull()
        val uid = uidUser.toRequestBody(stringMediaType)
        val fotoDisposable = AuthService.create()
                //.tambahFoto(uidUser, ktpFotoBase64, userFotoBase64)
                .uploadFotoSelfieKtp(uid,filePart)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            val fotoSelfieDisposable = AuthService.create()
                                //.tambahFoto(uidUser, ktpFotoBase64, userFotoBase64)
                                .uploadFotoSelfie(uid,filePart2)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                    {
                                        startActivity<SelesaiActivity>()
                                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                                        alertUpload.hide()
                                        finish()
                                    },
                                    { throwable ->
                                        Log.e(TAG, throwable.message)
                                        toast("Mohon cek koneksi anda")
                                        alertUpload.hide()
                                    }
                                )
                            compositeDisposable.add(fotoSelfieDisposable)
                        },
                        { throwable ->
                            Log.e(TAG, throwable.message)
                            toast("Mohon cek koneksi anda")
                            alertUpload.hide()
                        }
                )
        compositeDisposable.add(fotoDisposable)
    }

    private fun getFotoToBase64(fotoKtpPath: String?): String {
        val bitmapFactoryOptions = BitmapFactory.Options()
        var bitmapKtp = BitmapFactory.decodeFile(fotoKtpPath, bitmapFactoryOptions)

        if (bitmapKtp.width > App.FOTO_MAX_WIDTH_OR_HEIGHT || bitmapKtp.height > App.FOTO_MAX_WIDTH_OR_HEIGHT) {
            bitmapKtp = bitmapKtp.scale(800)
        }
        return bitmapToBase64(bitmapKtp)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            Activity.RESULT_OK -> {
                if (requestCode == RC_CAMERA_USER) {
                    val fotoPath = data?.getStringExtra("foto-path")
                    Log.d(TAG,fotoPath)
                    val bitmapFactoryOptions = BitmapFactory.Options()
                    var bitmap = BitmapFactory.decodeFile(fotoPath, bitmapFactoryOptions)

                    if (bitmap.width > App.FOTO_MAX_WIDTH_OR_HEIGHT || bitmap.height > App.FOTO_MAX_WIDTH_OR_HEIGHT) {
                        bitmap = bitmap.scale(800)
                    }

                    val file_path = Environment.getExternalStorageDirectory().absolutePath +
                            "/PhysicsSketchpad"
                    val dir = File(file_path)
                    if (!dir.exists()) dir.mkdirs()
                    val fotoname= Calendar.getInstance().getTime().toString()
                    val file = File(dir, fotoname + ".png")
                    val fOut = FileOutputStream(file)

                    bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut)
                    fOut.flush()
                    fOut.close()
                    dataRegisterSharedPref.fotoUserPath = file.absolutePath

                    fabPreviewUser.setOnClickListener {
                        alert {
                            customView {
                                linearLayout {
                                    lparams(matchParent, matchParent)
                                    zoomableImageView {
                                        backgroundColor = getColor(
                                                this@FotoKtpActivity,
                                                R.color.material_light_black
                                        )
                                        imageBitmap = bitmap
                                    }.lparams(matchParent, matchParent)
                                }
                            }
                        }.show()
                    }

                    imgUserPreview.imageBitmap = bitmap
                    if (imgKtpPreview.image != null)
                        btnRegisterLanjut.isEnabled = true

                } else if (requestCode == RC_CAMERA_KTP) {
                    val fotoPath = data?.getStringExtra("foto-path")
                    val bitmapFactoryOptions = BitmapFactory.Options()
                    var bitmap = BitmapFactory.decodeFile(fotoPath, bitmapFactoryOptions)
                    Log.d(TAG,fotoPath)
                    if (bitmap.width > App.FOTO_MAX_WIDTH_OR_HEIGHT || bitmap.height > App.FOTO_MAX_WIDTH_OR_HEIGHT) {
                        bitmap = bitmap.scale(800)
                    }
                    val file_path = Environment.getExternalStorageDirectory().absolutePath +
                            "/PhysicsSketchpad"
                    val dir = File(file_path)
                    if (!dir.exists()) dir.mkdirs()
                    val fotoname= Calendar.getInstance().getTime().toString()
                    val file = File(dir, fotoname + ".png")
                    val fOut = FileOutputStream(file)

                    bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut)
                    fOut.flush()
                    fOut.close()
                    dataRegisterSharedPref.fotoKtpPath = file.absolutePath
                    fabPreviewKtp.setOnClickListener {
                        alert {
                            customView {
                                linearLayout {
                                    lparams(matchParent, matchParent)
                                    zoomableImageView {
                                        backgroundColor = getColor(
                                                this@FotoKtpActivity,
                                                R.color.material_light_black
                                        )
                                        imageBitmap = bitmap
                                    }.lparams(matchParent, matchParent)
                                }
                            }
                        }.show()
                    }

                    imgKtpPreview.imageBitmap = bitmap
                    if (imgUserPreview.image != null)
                        btnRegisterLanjut.isEnabled = true
                }
            }
            Activity.RESULT_CANCELED -> longToast("Ada masalah image picker")
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }
}
