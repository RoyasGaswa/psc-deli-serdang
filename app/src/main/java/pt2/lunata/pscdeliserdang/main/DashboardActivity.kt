package pt2.lunata.pscdeliserdang.main

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.LocationManager
import android.os.*
import pt2.lunata.pscdeliserdang.R
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import com.bumptech.glide.Glide
import com.google.android.gms.location.*
import com.google.android.material.navigation.NavigationView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.activity_navbar.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.*
import pt2.lunata.pscdeliserdang.App
import pt2.lunata.pscdeliserdang.BaseActivity
import pt2.lunata.pscdeliserdang.aduan.AdukanDiriSendiriActivity
import pt2.lunata.pscdeliserdang.aduan.AdukanOrangLainActivity
import pt2.lunata.pscdeliserdang.aduan.SplashInfoPesananActivity
import pt2.lunata.pscdeliserdang.api.AduanService
import pt2.lunata.pscdeliserdang.api.AuthService
import pt2.lunata.pscdeliserdang.api.ProfilService
import pt2.lunata.pscdeliserdang.preauth.LandingActivity
import java.util.*

@SuppressLint("SetTextI18n")
class DashboardActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    companion object {
        const val RP_FINE_LOCATION = 1001
        const val LOCATION_UPDATE_INTERVAL = 60000L
        const val LOCATION_FASTEST_INTERVAL = 2000L
    }
    var long=""
    var lat=""
    private val compositeDisposable = CompositeDisposable()
    private val TAG = javaClass.simpleName
    private val locationCallback by lazy {
        object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                val location = locationResult?.lastLocation
                val geocoder = Geocoder(this@DashboardActivity, Locale.getDefault())
                val address = geocoder.getFromLocation(location!!.latitude, location.longitude, 1)
                userPref.lokasiSekarang = address[0].getAddressLine(0)
                userPref.koordinatSekarang = "${location.latitude},${location.longitude}"
                long=location.longitude.toString()
                lat=location.latitude.toString()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    override fun onStop() {
        super.onStop()
        FusedLocationProviderClient(this@DashboardActivity).removeLocationUpdates(locationCallback)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navbar)

        setSupportActionBar(toolbar)
        appBar.outlineProvider = null
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbarTitle.visibility = View.VISIBLE
        toolbarTitle.textColor = pt2.lunata.pscdeliserdang.getColor(this, R.color.pscAccent)
        toolbarTitle.text = "PUBLIC SAFETY CENTER"
        toolbar.backgroundColor = pt2.lunata.pscdeliserdang.getColor(this, android.R.color.transparent)
        appBar.backgroundColor = pt2.lunata.pscdeliserdang.getColor(this, android.R.color.transparent)

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        val logoutItem = navView.menu.findItem(R.id.nav_logout)
        val redText = SpannableString(logoutItem.title.toString())
        redText.setSpan(
            ForegroundColorSpan(pt2.lunata.pscdeliserdang.getColor(this@DashboardActivity, R.color.material_red_500)),
            0,
            logoutItem.title.toString().length,
            0
        )
        toggle.drawerArrowDrawable.color =
                pt2.lunata.pscdeliserdang.getColor(this@DashboardActivity, R.color.material_light_white)

        navView.setNavigationItemSelectedListener(this)

        navView.getHeaderView(0).also { view ->
            view.findViewById<TextView>(R.id.textNama).text = userPref.nama
            view.findViewById<TextView>(R.id.textHp).text = userPref.hp

            val imgView = view.findViewById<ImageView>(R.id.imgProfil)
            Glide.with(this@DashboardActivity).load(userPref.fotoUrl).into(imgView)
        }

        sendBroadcast(Intent(App.BC_SELESAI))
        sendBroadcast(Intent(App.BC_VERIFIKASI))
        sendBroadcast(Intent(App.BC_FOTOKTP))
        sendBroadcast(Intent(App.BC_REGISTER))
        sendBroadcast(Intent(App.BC_LANDING))
        getLocation()
        swipeRefreshLayout.isRefreshing = true
        swipeRefreshLayout.setOnRefreshListener {
            cekAduan()
        }
        btnRefresh.setOnClickListener {
            cekAduan()
        }

        setButton()
    }

    override fun onResume() {
        super.onResume()

        if (ActivityCompat.checkSelfPermission(
                this, android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            disableAduan("Mohon izinkan akses lokasi")
            ActivityCompat.requestPermissions(
                this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), RP_FINE_LOCATION
            )
        }

        cekAduan()
    }

    private fun cekAduan() {
        disableAduan("Cek Pengaduan...")

        if (userPref.idStatus == null) {
            disableAduan("Akun Anda Belum Diverifikasi Admin")
            val cekVerifikasiDisposable = ProfilService.create()
                .userData("Bearer "+userPref.token,userPref.uid!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->
                        userPref.idStatus = result.data.approved
                        if (result.data.approved != null) enableAduan()

                        if (result.data.approved == null) disableAduan("Yeay Akun anda ditolak oleh admin")

                        swipeRefreshLayout.isRefreshing = false
                    },
                    { throwable ->
                        swipeRefreshLayout.isRefreshing = false
                        Log.e(TAG, throwable.message)
                    }
                )
            compositeDisposable.add(cekVerifikasiDisposable)
        } else if (userPref.idStatus == null) {
            disableAduan("Akun anda ditolak oleh admin")
            swipeRefreshLayout.isRefreshing = false
        } else {
            val cekAduanDisposable = AduanService.create()
                .cekAduan("Bearer "+userPref.token!!,userPref.uid!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->
                        if (result.message != "Belum ada laporan berjalan !" && result.message!="Ambulance telah tiba dirumah sakit !") {
                            disableAduan(result.message)
                            Log.d(TAG,result.message)
                        } else {
                            enableAduan()
                            getLocation()
                            Log.d(TAG,userPref.lokasiSekarang.toString())
                        }
                        swipeRefreshLayout.isRefreshing = false
                    },
                    { throwable ->
                        disableAduan("Mohon Cek Koneksi Anda")
                        swipeRefreshLayout.isRefreshing = false
                        Log.e(TAG, throwable.message)
                    }
                )
            compositeDisposable.add(cekAduanDisposable)
        }
    }

    private fun setButton() {
        btnLaporDiriSendiri.setOnClickListener {
            startActivity<AdukanDiriSendiriActivity>()
        }

        btnLaporOrangLain.setOnClickListener {
            startActivity<AdukanOrangLainActivity>()
        }

        val runPanicAction = Runnable {
            btnPanic.setImageDrawable(
                pt2.lunata.pscdeliserdang.getDrawable(
                    this@DashboardActivity,
                    R.drawable.btn_panic_steady
                )
            )

            val nama = userPref.nama!!
            Log.d(TAG,"Long = "+long.toString())
            val alamat = userPref.lokasiSekarang!!

            val koordinat = userPref.koordinatSekarang!!
            val uidUser = userPref.uid!!
            //val uidJenkel = userPref.uidKelamin!!
            //val umur = calculateAge(userPref.tglLahir!!.toDate("yyyy-MM-dd")).years

            val aduanDisposable = AduanService.create()
                .panic("Bearer "+userPref.token!!,uidUser,alamat,long,lat,"3")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        startActivity<SplashInfoPesananActivity>()
                    },
                    { throwable ->
                        Log.e(TAG, throwable.message)
                        longToast("Mohon Cek Koneksi Anda")
                    }
                )

            compositeDisposable.add(aduanDisposable)
        }

        val handler = Handler()
        btnPanic.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                    vibratorService.vibrate(300)

                    (v as ImageButton).setImageDrawable(
                        pt2.lunata.pscdeliserdang.getDrawable(
                            this@DashboardActivity,
                            R.drawable.btn_panic_ok
                        )
                    )
                    handler.postDelayed(runPanicAction, 3000)
                }
                MotionEvent.ACTION_MOVE -> {

                }
                else -> {
                    handler.removeCallbacks(runPanicAction)
                    (v as ImageButton).setImageDrawable(
                        pt2.lunata.pscdeliserdang.getDrawable(this@DashboardActivity, R.drawable.btn_panic_steady)
                    )
                }
            }
            return@setOnTouchListener true
        }
    }

    private fun enableAduan() {
        textPesan.visibility = View.GONE

        btnLaporDiriSendiri.isEnabled = true
        btnLaporOrangLain.isEnabled = true
        btnPanic.isEnabled = true

        btnLaporDiriSendiri.alpha = 1F
        labelBtnLaporSendiri.alpha = 1F
        btnLaporOrangLain.alpha = 1F
        labelBtnLaporOrang.alpha = 1F
        btnPanic.alpha = 1F
        infoBtnPanic.alpha = 1F
    }

    private fun disableAduan(message: String = "") {
        textPesan.visibility = View.VISIBLE
        textPesan.text = message

        btnLaporDiriSendiri.isEnabled = false
        btnLaporOrangLain.isEnabled = false
        btnPanic.isEnabled = false

        btnLaporDiriSendiri.alpha = .5F
        labelBtnLaporSendiri.alpha = .5F
        btnLaporOrangLain.alpha = .5F
        labelBtnLaporOrang.alpha = .5F
        btnPanic.alpha = .5F
        infoBtnPanic.alpha = .5F
    }


    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_history -> {
                startActivity<ListAduanActivity>()
            }
            /*R.id.nav_list_rumah_sakit -> {
                startActivity<ListLayananActivity>()
            }*/
            R.id.nav_akun -> {
                startActivity<DetailProfilActivity>()
            }
            /*R.id.nav_bantuan -> {
                startActivity<WebviewActivity>(
                    "activity-title" to "Bantuan",
                    "url" to "https://dinkesmedan.lunata.co.id/psc/viewbantuan.php"
                )
            }*/
            R.id.nav_terms -> {
                startActivity<WebviewActivity>(
                    "activity-title" to "Syarat dan Ketentuan",
                    "url" to "https://dinkesmedan.lunata.co.id/psc/viewterms.php"
                )
            }
            /*R.id.nav_privacy -> {
                startActivity<WebviewActivity>(
                    "activity-title" to "Kebijakan Layanan",
                    "url" to "https://dinkesmedan.lunata.co.id/psc/viewprivacy.php"
                )
            }*/
            R.id.nav_logout -> {
                val logoutDisposable = AuthService.create()
                    .logout(userPref.uid!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        {
                            userPref.logout()
                            startActivity<LandingActivity>()
                            finish()
                        },
                        { throwable ->
                            Log.e(TAG, throwable.message)
                            toast("Mohon cek koneksi anda")
                        }
                    )

                compositeDisposable.add(logoutDisposable)
            }
        }

        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            RP_FINE_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG,"Request masuk")
                    getLocation()
                    enableAduan()
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLocation() {
        val lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (!lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER) && !lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            disableAduan("Mohon aktifkan GPS anda")
        } else {
            val locationRequest = LocationRequest();
            locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
            locationRequest.interval = LOCATION_UPDATE_INTERVAL
            locationRequest.fastestInterval = LOCATION_FASTEST_INTERVAL

            val locationSettingRequest = LocationSettingsRequest.Builder().apply {
                addLocationRequest(locationRequest)
            }.build()

            LocationServices.getSettingsClient(this@DashboardActivity).apply {
                checkLocationSettings(locationSettingRequest)
            }
            Log.d(TAG,"masuk Geo")
            FusedLocationProviderClient(this@DashboardActivity).requestLocationUpdates(
                locationRequest, locationCallback, null
            )
        }
    }
}
