package pt2.lunata.pscdeliserdang.api

import pt2.lunata.pscdeliserdang.App
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {
    /**
     *  Pastikan class T itu interface dari service retrofit
     * */
    fun <T> create(service: Class<T>): T {
        return Retrofit.Builder()
                .baseUrl(App.API)
                .callFactory(App.httpClientBuilder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(App.GSON))
                .build()
                .create(service)
    }
}
