package pt2.lunata.pscdeliserdang.api

import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pt2.lunata.pscdeliserdang.api.model.Aduan
import retrofit2.http.*

interface AduanService {
    @GET("diseases")
    fun listPenyakit(): Observable<Aduan.ListItemResponse>

    @GET("hubkel")
    fun listHubkel(): Observable<Aduan.ListHubkelResponse>

    @FormUrlEncoded
    @POST("report-status")
    fun cekAduan(
            @Header("Authorization") token: String,
            @Field("uid")uid:String
    ): Observable<Aduan.MessageResponse>

    @Multipart
    @POST("people-report")
    fun adukanDiriSendiri(
        @Header("Authorization")token:String,
        @Part("people_uid") uid: RequestBody,
        @Part("diseases_id") uidJenisPenyakit: RequestBody,
        @Part("report_type") reportType:RequestBody,
        @Part("description") keterangan: RequestBody,
        @Part("address") alamat: RequestBody,
        @Part("village_id")kelurahan:RequestBody,
        @Part("longtitude") longtitude: RequestBody,
        @Part("latitude") latitude: RequestBody,
        @Part image1:MultipartBody.Part
    ): Observable<Aduan.MessageResponse>

    @Multipart
    @POST("people-report")
    fun adukanOrangLain(
        @Header("Authorization")token:String,
        @Part("people_uid") uid: RequestBody,
        @Part("other_people_name") nama: RequestBody,
        @Part("other_people_age")umur : RequestBody,
        @Part("other_people_gender") kelamin : RequestBody,
        @Part("diseases_id") uidJenisPenyakit: RequestBody,
        @Part("report_type") reportType:RequestBody,
        @Part("description") keterangan: RequestBody,
        @Part("address") alamat: RequestBody,
        @Part("village_id")kelurahan:RequestBody,
        @Part("longtitude") longtitude: RequestBody,
        @Part("latitude") latitude: RequestBody,
        @Part image1:MultipartBody.Part
    ): Observable<Aduan.MessageResponse>

    @FormUrlEncoded
    @POST("people-report")
    fun panic(
            @Header("Authorization")token:String,
            @Field("people_uid") uidUser: String,
            @Field("address") alamat: String,
            @Field("longtitude") longtitude: String,
            @Field("latitude")latitude:String,
            @Field("report_type")reportType:String
    ): Observable<Aduan.MessageResponse>

    @FormUrlEncoded
    @POST("laporan-log")
    fun history(
        @Header("Authorization") token: String,
        @Field("people_uid")uid:String,
        @Field("status")status:String)
            : Observable<Aduan.HistoryResponse>

    @GET("aduan/detail")
    fun detail(@Query("uidAduan") uidAduan: String)
            : Observable<Aduan.DetailResponse>

    companion object factory {
        fun create(): AduanService = RetrofitBuilder.create(AduanService::class.java)
    }
}