package pt2.lunata.pscdeliserdang.helper;


import androidx.annotation.NonNull;

public class Age {
    private int days;
    private int months;
    private int years;

    private Age() {
        //Prevent default constructor
    }

    Age(int days, int months, int years) {
        this.days = days;
        this.months = months;
        this.years = years;
    }

    public int getDays() {
        return this.days;
    }

    public int getMonths() {
        return this.months;
    }

    public int getYears() {
        return this.years;
    }

    @NonNull
    @Override
    public String toString() {
        return years + " Years, " + months + " Months, " + days + " Days";
    }
}
