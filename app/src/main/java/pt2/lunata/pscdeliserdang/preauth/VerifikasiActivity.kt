package pt2.lunata.pscdeliserdang.preauth

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import kotlinx.android.synthetic.main.register_step.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.backgroundColor
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.poovam.pinedittextfield.PinField
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_verifikasi.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import pt2.lunata.pscdeliserdang.App
import pt2.lunata.pscdeliserdang.BaseActivity
import pt2.lunata.pscdeliserdang.R
import pt2.lunata.pscdeliserdang.textViewMakeLinks
import pt2.lunata.pscdeliserdang.api.AuthService

class VerifikasiActivity : BaseActivity() {
    companion object {
        const val CURRENT_BROADCAST = App.BC_VERIFIKASI
    }

    val dataRegisterSharedPref = DataRegisterSharedPref()
    val compositeDisposable = CompositeDisposable()
    val TAG = this::class.java.simpleName

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.action
            if (action.equals(CURRENT_BROADCAST)) {
                finish()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
        compositeDisposable.dispose()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verifikasi)

        registerReceiver(broadcastReceiver, IntentFilter(CURRENT_BROADCAST))

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        appBar.outlineProvider = null
        toolbar.backgroundColor = pt2.lunata.pscdeliserdang.getColor(
            this,
            R.color.pscPrimary
        )

        registerStepView.statusView.currentCount = 3

        val kirimVerifikasiDisposable = AuthService.create()
                .kirimVerifikasi(dataRegisterSharedPref.uid!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->

                        },
                        { throwable ->
                            toast("Ada masalah")
                            Log.e(TAG, throwable.message)
                        }
                )
        compositeDisposable.add(kirimVerifikasiDisposable)

        val links = arrayOf("Belum menerima kode?")
        val linksAction: Array<ClickableSpan> = arrayOf(
                object : ClickableSpan() {
                    override fun onClick(textView: View) {
                        val kirimUlangVerifikasiDisposable = AuthService.create()
                                .kirimVerifikasi(dataRegisterSharedPref.uid!!)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        { result ->
                                            toast("Silahkan Cek email anda")
                                        },
                                        { throwable ->
                                            toast("Ada masalah")
                                            Log.e(TAG, throwable.message)
                                        }
                                )
                        compositeDisposable.add(kirimUlangVerifikasiDisposable)
                        textView.invalidate()
                    }

                    override fun updateDrawState(ds: TextPaint) {
                        super.updateDrawState(ds)
                        ds.isUnderlineText = true
                        ds.color = pt2.lunata.pscdeliserdang.getColor(this@VerifikasiActivity, R.color.material_light_white)
                    }
                }
        )

        textViewMakeLinks(textInfoVerifikasiAkun, links, linksAction)

        inputVerification.requestFocus()

        // nunggu animasi baru tampilkan keyboard
        Handler().postDelayed({
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
        }, this.resources.getInteger(R.integer.slide_animation_duration).toLong())

        inputVerification.onTextCompleteListener = object : PinField.OnTextCompleteListener {
            override fun onTextComplete(enteredText: String): Boolean {
                layoutWarning.visibility = View.GONE

                val verifikasiDisposable = AuthService.create()
                        .verifikasi(dataRegisterSharedPref.uid!!, enteredText)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    startActivity<SelesaiActivity>()
                                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                                },
                                { throwable ->
                                    Log.e(TAG, throwable.message)
                                    layoutWarning.visibility = View.VISIBLE
                                }
                        )
                compositeDisposable.add(verifikasiDisposable)
                return true
            }
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }
}
