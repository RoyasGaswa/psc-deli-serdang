package pt2.lunata.pscdeliserdang.preauth

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import org.jetbrains.anko.startActivity
import pt2.lunata.pscdeliserdang.R
import pt2.lunata.pscdeliserdang.UserSharedPref
import pt2.lunata.pscdeliserdang.main.DashboardActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        supportActionBar?.hide()

        window.decorView.apply {
            systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }

        Handler().postDelayed({
            if (UserSharedPref().isLoggedIn()) startActivity<DashboardActivity>()
            else startActivity<LandingActivity>()
            finish()
        }, 1000)
    }
}
