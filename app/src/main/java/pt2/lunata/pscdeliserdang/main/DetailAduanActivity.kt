package pt2.lunata.pscdeliserdang.main

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import khronos.toDate
import kotlinx.android.synthetic.main.activity_detail_aduan.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.*
import pt2.lunata.pscdeliserdang.*
import pt2.lunata.pscdeliserdang.api.AduanService
import pt2.lunata.pscdeliserdang.api.model.Aduan
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class DetailAduanActivity : BaseActivity() {

    val compositeDisposable = CompositeDisposable()
    private lateinit var uidAduan: String
    private lateinit var data : Aduan.History
    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_aduan)

        uidAduan = ""
        data=intent.getParcelableExtra<Aduan.History>("extra_data")

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbarTitle.text = "Detail Aduan"
        toolbarTitle.textColor = pt2.lunata.pscdeliserdang.getColor(this, R.color.pscAccent)
        toolbarTitle.visibility = View.VISIBLE
        toolbar.backgroundColor = pt2.lunata.pscdeliserdang.getColor(this, android.R.color.transparent)
        appBar.backgroundColor = pt2.lunata.pscdeliserdang.getColor(this, android.R.color.transparent)

        appBar.outlineProvider = null
        //setupView()
        populateData()
    }

    private fun populateData(){
        if (::data.isInitialized){
            dataTanggalAduan.text = formatDate(data.created_at)!!
            dataJenisAduan.text = data.report_type
            dataJenisPenyakit.text = data.diseases_name
            //dataKeterangan.text = data.keterangan
            dataNamaKorban.text = data.reporter_name
            dataUmur.text = data.people_appends.age
            dataJenisKelamin.text = data.people_appends.gender

            /*if (data.hubkel != "Diri Sendiri") {
                dataHubunganKorban.visibility = View.VISIBLE
                dataHubunganKorban.text = data.hubkel
            }*/
        }
    }
    private fun formatDate(tanggal: String): String? {
        val input = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val output = SimpleDateFormat("dd MMM yyyy")
        var d: Date? = null
        try {
            d = input.parse(tanggal)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return output.format(d)
    }

    private fun setupView() {
        if (::data.isInitialized) {
            val historyDisposable = AduanService.create()
                    .detail(uidAduan)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { result ->
                                val data = result.data

                                dataTanggalAduan.text = data.tanggalAduan.toDate(App.DEFAULT_DATABASE_TIME_FORMAT).toStringIndo("HH:mm d/MMM/yyyy")
                                dataJenisAduan.text = data.tipeAduan
                                dataJenisPenyakit.text = data.jenisPenyakit
                                //dataKeterangan.text = data.keterangan
                                dataNamaKorban.text = data.nama
                                dataUmur.text = data.umur
                                dataJenisKelamin.text = data.jenisKelamin

                                if (data.hubkel != "Diri Sendiri") {
                                    dataHubunganKorban.visibility = View.VISIBLE
                                    dataHubunganKorban.text = data.hubkel
                                }

                                /*dataStatus.text = data.statusPengaduan

                                inputLokasiKejadian.text = data.alamatKejadian
                                inputLokasiKejadian.value = data.koorKejadian
                                inputLokasiKejadian.icon.setImageResource(R.drawable.ic_location_on_black_24dp)

                                inputLokasiKejadian.inputText.setOnClickListener {
                                    openMap(data.koorKejadian, data.alamatKejadian)
                                }

                                if (data.alamatRumahSakit != null && data.koorRumahSakit != null) {
                                    inputLokasiRumahSakit.visibility = View.VISIBLE
                                    inputLokasiRumahSakit.icon.setImageResource(R.drawable.ic_location_on_black_24dp)
                                    inputLokasiRumahSakit.textNullable = data.alamatRumahSakit
                                    inputLokasiRumahSakit.value = data.koorRumahSakit

                                    inputLokasiRumahSakit.inputText.setOnClickListener {
                                        openMap(data.koorRumahSakit, data.alamatRumahSakit)
                                    }
                                }

                                Glide.with(this@DetailAduanActivity).asBitmap().load(data.foto)
                                        .into(object : SimpleTarget<Bitmap>() {
                                            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                                imgFotoPreview.imageBitmap = resource
                                            }

                                        })

                                fabPreviewFoto.setOnClickListener {
                                    alert {
                                        customView {
                                            linearLayout {
                                                lparams(matchParent, matchParent)
                                                zoomableImageView {
                                                    backgroundColor = getColor(
                                                            this@DetailAduanActivity,
                                                            R.color.material_light_black
                                                    )
                                                    Glide.with(this@DetailAduanActivity).asBitmap().load(data.foto)
                                                            .into(object : SimpleTarget<Bitmap>() {
                                                                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                                                    imgFotoPreview.imageBitmap = resource
                                                                }
                                                            })
                                                }.lparams(matchParent, matchParent)
                                            }
                                        }
                                    }.show()
                                }*/

                            },
                            { throwable ->
                                Log.e(TAG, throwable.message)
                                toast("Mohon cek koneksi anda")
                            }
                    )

            compositeDisposable.add(historyDisposable)
        } else {
            toast("Ada masalah dengan detail")
        }
    }

    private fun openMap(koor: String, alamat: String) {
        val latLong = koor.replace("\\s".toRegex(), "").split(",").map {
            it.toDouble()
        }
        val uriMaps = String.format(Locale.ENGLISH, "geo:0,0?q=%f,%f(%s)", latLong[0], latLong[1], alamat)
        val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse(uriMaps))
        startActivity(mapIntent)
    }
}
