package pt2.lunata.pscdeliserdang.main

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.history_item.view.*
import pt2.lunata.pscdeliserdang.R
import pt2.lunata.pscdeliserdang.api.model.Aduan
import pt2.lunata.pscdeliserdang.inflate
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ListAduanAdapter(
    val fullList: List<Aduan.History>? = null,
    val listener: ((Aduan.History) -> Unit)? = null
) : RecyclerView.Adapter<ListAduanAdapter.ViewHolder>() {
    private val placeholder = fullList == null
    private var mainList = fullList?.toList() ?: listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.history_item))
    }

    override fun getItemCount() = if (placeholder) 9 else mainList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (!placeholder) holder.bind(mainList[position], listener)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Aduan.History, listener: ((Aduan.History) -> Unit)?) = with(itemView) {
            textJenisPenyakit.text = item.diseases_name
            textWaktu.text = formatDate(item.created_at)
            if (item.report_type=="3"){
                textTipeAduan.text="Panic Button"
            }else if (item.report_type=="2"){
                textTipeAduan.text = "Pengaduan Orang Lain"
            }else{
                textTipeAduan.text = "Pengaduan Diri Sendiri"
            }

            textStatusAduan.text = item.address

            layoutContainer.setOnClickListener {
                listener?.invoke(item)
            }
        }

        /* @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }*/
        private fun formatDate(tanggal: String): String? {
            val input = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            val output = SimpleDateFormat("dd MMM yyyy")
            var d: Date? = null
            try {
                d = input.parse(tanggal)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return output.format(d)
        }
    }
}
