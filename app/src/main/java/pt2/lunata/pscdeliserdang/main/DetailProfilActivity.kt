package pt2.lunata.pscdeliserdang.main

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import io.reactivex.disposables.CompositeDisposable
import khronos.toDate
import kotlinx.android.synthetic.main.activity_detail_profil.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.*
import pt2.lunata.pscdeliserdang.*

class DetailProfilActivity : BaseActivity() {

    val compositeDisposable = CompositeDisposable()
    val TAG = this::class.java.simpleName

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_profil)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbarTitle.text = "Profil"
        toolbarTitle.textColor = pt2.lunata.pscdeliserdang.getColor(this@DetailProfilActivity, R.color.pscAccent)
        toolbarTitle.visibility = View.VISIBLE

        appBar.outlineProvider = null
        toolbar.backgroundColor = pt2.lunata.pscdeliserdang.getColor(this, android.R.color.transparent)
        appBar.backgroundColor = pt2.lunata.pscdeliserdang.getColor(this, android.R.color.transparent)
    }

    override fun onResume() {
        super.onResume()

        setupForm()
    }

    private fun setupForm() {
        dataNama.text = userPref.nama!!
        //dataEmail.text = userPref.email!
        //dataTanggalLahir.text = userPref.tglLahir!!.toDate("yyyy-MM-dd").toStringIndo("dd MMMM yyyy")
        //dataKtp.text = data.ktp
        dataTanggalLahir.text =userPref.umur!!
        dataHp.text = userPref.hp!!
        dataAlamat.text = userPref.lokasiSekarang!!
        if (userPref.uidKelamin=="M"){
            dataKelamin.text ="Laki-Laki"
        }else{
            dataKelamin.text ="Perempuan"
        }

        /*dataGoldar.text = data.goldar
        dataStatusUser.text = data.statusUser
        dataBpjs.text = data.bpjs*/
        dataKecamatan.text = userPref.kecamatan!!
        dataKelurahan.text = userPref.kelurahan!!

        textNamaUser.text = userPref.nama
        Glide.with(this@DetailProfilActivity).load(userPref.fotoUrl).into(imgFotoUser)

        imgFotoUser.setOnClickListener {
            alert {
                customView {
                    linearLayout {
                        lparams(matchParent, matchParent)
                        zoomableImageView {
                            backgroundColor = getColor(
                                this@DetailProfilActivity,
                                R.color.material_light_black
                            )
                            Glide.with(this@DetailProfilActivity).asBitmap().load(userPref.fotoUrl)
                                .into(object : SimpleTarget<Bitmap>() {
                                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                        imgFotoUser.imageBitmap = resource
                                    }
                                })
                        }.lparams(matchParent, matchParent)
                    }
                }
            }.show()
        }
        btnEdit.setOnClickListener {
            startActivity<EditProfilActivity>()
        }

        btnGantiPassword.setOnClickListener {
            startActivity<GantiPasswordActivity>()
        }
        /*val detailDisposable = ProfilService.create()
                .detail(userPref.uid!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            val data = result.data

                            dataNama.text = data.nama
                            dataEmail.text = data.email
                            dataTanggalLahir.text = data.tanggalLahir.toDate("yyyy-MM-dd").toStringIndo("dd MMMM yyyy")
                            //dataKtp.text = data.ktp
                            dataHp.text = data.noHp
                            dataAlamat.text = data.alamat
                            dataKelamin.text = data.kelamin
                            *//*dataGoldar.text = data.goldar
                            dataStatusUser.text = data.statusUser
                            dataBpjs.text = data.bpjs*//*
                            dataKecamatan.text = data.kecamatan
                            dataKelurahan.text = data.kelurahan

                            textNamaUser.text = data.nama
                            Glide.with(this@DetailProfilActivity).load(data.fotoUrl).into(imgFotoUser)

                            imgFotoUser.setOnClickListener {
                                alert {
                                    customView {
                                        linearLayout {
                                            lparams(matchParent, matchParent)
                                            zoomableImageView {
                                                backgroundColor = getColor(
                                                        this@DetailProfilActivity,
                                                        R.color.material_light_black
                                                )
                                                Glide.with(this@DetailProfilActivity).asBitmap().load(data.fotoUrl)
                                                        .into(object : SimpleTarget<Bitmap>() {
                                                            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                                                imgFotoUser.imageBitmap = resource
                                                            }
                                                        })
                                            }.lparams(matchParent, matchParent)
                                        }
                                    }
                                }.show()
                            }

                            btnEdit.setOnClickListener {
                                startActivity<EditProfilActivity>()
                            }

                            btnGantiPassword.setOnClickListener {
                                startActivity<GantiPasswordActivity>()
                            }
                        },
                        { throwable ->
                            Log.e(TAG, throwable.message)
                            toast("Ada masalah")
                        }
                )
        compositeDisposable.add(detailDisposable)*/
    }
}
