package pt2.lunata.pscdeliserdang

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_open_maps.*
import org.jetbrains.anko.activityUiThread
import org.jetbrains.anko.doAsync
import java.util.*

class OpenMapsActivity : BaseActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
    GoogleMap.OnMapClickListener {
    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var geocoder: Geocoder
    private lateinit var address: List<Address>

    companion object {
        private const val RQ_LOCATION_PERMISSION = 10
        private const val RQ_ACCESS_FINE_LOCATION = android.Manifest.permission.ACCESS_FINE_LOCATION
    }

    override fun onMarkerClick(p0: Marker?): Boolean = false

    override fun onMapClick(clickedPosition: LatLng) = setMarker(clickedPosition)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_maps)

        supportActionBar?.hide()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = Color.TRANSPARENT
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        geocoder = Geocoder(this@OpenMapsActivity, Locale.getDefault())

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.setPadding(0, 42, 0, bottomFrame.height)
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.setOnMarkerClickListener(this)
        mMap.setOnMapClickListener(this)

        setUpMapIf()
    }

    private fun setUpMapIf() {
        if (ActivityCompat.checkSelfPermission(
                this,
                RQ_ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(RQ_ACCESS_FINE_LOCATION), RQ_LOCATION_PERMISSION
            )
            return
        }

        mMap.isMyLocationEnabled = true

        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                setMarker(currentLatLng)
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 15f))
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setMarker(location: LatLng) {
        layoutShimmer.visibility = View.VISIBLE
        layoutAddress.visibility = View.GONE
        btnSetPickupLocation.visibility = View.GONE
        mMap.setPadding(0, 42, 0, bottomFrame.height)
        mMap.clear()
        mMap.addMarker(MarkerOptions().position(location))
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, mMap.cameraPosition.zoom))

        doAsync {
            address = geocoder.getFromLocation(location.latitude, location.longitude, 1)
            activityUiThread {
                val currentAddress = address[0]
                var addressNumber = ""
                layoutAddress.visibility = View.VISIBLE
                btnSetPickupLocation.visibility = View.VISIBLE
                layoutShimmer.visibility = View.GONE

                if (currentAddress.subThoroughfare != null) addressNumber = "No. ${currentAddress.subThoroughfare}"

                // TODO: kadang thoroughtFare return null. Harusnya return tempat terdekat
                textShortLocation.text = "${currentAddress.thoroughfare} ${addressNumber}"
                textLongLocation.text = currentAddress.getAddressLine(0)
            }
        }
    }

    fun setPickupLocation(view: View) {
        val result = Intent()
        result.putExtra(App.RI_MAP_ORIGIN, address[0])
        setResult(Activity.RESULT_OK, result)
        finish()
    }
}

