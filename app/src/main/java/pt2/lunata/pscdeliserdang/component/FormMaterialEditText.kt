package pt2.lunata.pscdeliserdang.component

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.text.InputType
import android.util.AttributeSet
import android.view.Gravity
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import com.google.android.material.textfield.TextInputLayout
import khronos.Dates
import khronos.toDate
import org.jetbrains.anko.lines
import pt2.lunata.pscdeliserdang.App
import pt2.lunata.pscdeliserdang.R
import pt2.lunata.pscdeliserdang.afterTextChanged

import java.text.DateFormatSymbols

@SuppressLint("SetTextI18n")
class FormMaterialEditText(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

    val textNullable: String?
        get() = if (inputText.text.isEmpty()) null else inputText.text.toString()

    val text: String
        get() = inputText.text.toString()

    val inputText: EditText
    val layoutInputText: TextInputLayout
    val icon: ImageView
    val labelEditText: TextInputLayout

    var datePickerDialog: DatePickerDialog? = null
    var year: Int
    var month: Int
    var day: Int
    var value: String? = null

    private val isTextEnabled: Boolean
    private val isRequired: Boolean
    private val isWrapText: Boolean
    private val isTextFocusable: Boolean
    private val isDatePicker: Boolean

    init {
        inflate(context, R.layout.component_form_material_edit_text, this)

        icon = findViewById(R.id.imgIcon)
        labelEditText = findViewById(R.id.layoutEditText)
        inputText = findViewById(R.id.editText)
        layoutInputText = findViewById(R.id.layoutEditText)

        @SuppressLint("CustomViewStyleable")
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.ComponentFormMaterialEditText)
        icon.setImageDrawable(attributes.getDrawable(R.styleable.ComponentFormMaterialEditText_icon))
        labelEditText.hint = attributes.getString(R.styleable.ComponentFormMaterialEditText_hint)
        isTextFocusable = attributes.getBoolean(R.styleable.ComponentFormMaterialEditText_focusable, true)
        isTextEnabled = attributes.getBoolean(R.styleable.ComponentFormMaterialEditText_enabled, true)
        isWrapText = attributes.getBoolean(R.styleable.ComponentFormMaterialEditText_wrapText, false)
        isRequired = attributes.getBoolean(R.styleable.ComponentFormMaterialEditText_required, false)

        isDatePicker = attributes.getBoolean(R.styleable.ComponentFormMaterialEditText_datePicker, false)
        year = attributes.getInt(R.styleable.ComponentFormMaterialEditText_year, 2000)
        month = attributes.getInt(R.styleable.ComponentFormMaterialEditText_month, 1)
        day = attributes.getInt(R.styleable.ComponentFormMaterialEditText_day, 1)

        inputText.apply {
            isFocusable = isTextFocusable && !isDatePicker
        }

        inputText.apply {
            isEnabled = isTextEnabled
        }

        if (isRequired) {
            inputText.afterTextChanged {
                if (inputText.text.isEmpty()) inputText.error = "Input dibutuhkan"
                else inputText.error = null
            }
        }

        if (isWrapText) {
            inputText.apply {
                lines = 3
                gravity = Gravity.TOP
                inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
            }
        }

        if (isDatePicker) {
            datePickerDialog = DatePickerDialog(context, DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                inputText.setText("$dayOfMonth ${DateFormatSymbols().shortMonths[monthOfYear]} $year")
                inputText.text?.toString()?.toDate(App.DEFAULT_DATE_FORMAT)
            }, year, month, day)
            datePickerDialog?.datePicker?.minDate = Dates.of(1930, 1, 1).time
        }

        attributes.recycle()
    }

    private fun validateRequired(): Boolean {
        return if (isRequired) {
            if (inputText.text.isEmpty()) {
                inputText.error = "Input Dibutuhkan"
                false
            } else true
        } else true
    }

    fun isValid(): Boolean = inputText.error == null && validateRequired()
}