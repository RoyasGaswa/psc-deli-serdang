package pt2.lunata.pscdeliserdang.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_ganti_password.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.textColor
import org.jetbrains.anko.toast
import pt2.lunata.pscdeliserdang.BaseActivity
import pt2.lunata.pscdeliserdang.R
import pt2.lunata.pscdeliserdang.api.ProfilService
import pt2.lunata.pscdeliserdang.getColor

class GantiPasswordActivity : BaseActivity() {

    val compositeDisposable = CompositeDisposable()
    val TAG = javaClass.simpleName

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ganti_password)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbarTitle.text = "Ganti Password"
        toolbarTitle.textColor = getColor(this@GantiPasswordActivity, R.color.pscAccent)
        toolbarTitle.visibility = View.VISIBLE
        toolbar.backgroundColor = getColor(this, android.R.color.transparent)

        appBar.outlineProvider = null
        appBar.backgroundColor = getColor(this, android.R.color.transparent)

        inputPassLama.layoutInputText.isPasswordVisibilityToggleEnabled = true
        inputPassBaru.layoutInputText.isPasswordVisibilityToggleEnabled = true
        inputPassBaruKonfirmasi.layoutInputText.isPasswordVisibilityToggleEnabled = true

        btnKirim.setOnClickListener {
            if (inputPassLama.isValid() && inputPassBaru.isValid() && inputPassBaruKonfirmasi.isValid()) {
                if (inputPassBaru.text == inputPassBaruKonfirmasi.text) {
                    btnKirim.isEnabled = false
                    val gantiPassDisposable = ProfilService.create()
                            .gantiPassword(userPref.uid!!, inputPassLama.text, inputPassBaru.text)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    {
                                        toast("Password Berhasil Diganti")
                                        finish()
                                    },
                                    { throwable ->
                                        btnKirim.isEnabled = true
                                        Log.e(TAG, throwable.message)
                                        toast("Mohon cek password lama anda")
                                    }
                            )
                    compositeDisposable.add(gantiPassDisposable)
                } else {
                    toast("Password baru tidak sama dengan password konfirmasi")
                }
            } else {
                toast("Harap Lengkapi Form")
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}
