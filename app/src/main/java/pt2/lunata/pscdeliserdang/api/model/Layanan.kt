package pt2.lunata.pscdeliserdang.api.model

object Layanan {

    data class LayananResponse(val data: List<Layanan>, val message: String)
    data class Layanan(
            val uid: String,
            val nama: String,
            val noHp: String,
            val alamat: String,
            val koordinat: String,
            val statusBpjs: String,
            val layananTipe: String
    )
}

