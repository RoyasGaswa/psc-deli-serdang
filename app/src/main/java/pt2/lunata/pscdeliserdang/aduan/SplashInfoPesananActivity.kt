package pt2.lunata.pscdeliserdang.aduan

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Vibrator
import android.view.View
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.backgroundColor
import pt2.lunata.pscdeliserdang.R
import pt2.lunata.pscdeliserdang.BaseActivity
import pt2.lunata.pscdeliserdang.getColor

class SplashInfoPesananActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_info_pesanan)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbarTitle.visibility = View.VISIBLE

        appBar.outlineProvider = null
        toolbar.backgroundColor = getColor(
                this,
                R.color.pscPrimary
        )

        toolbar.backgroundColor = getColor(this, android.R.color.transparent)
        appBar.backgroundColor = getColor(this, android.R.color.transparent)

        val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        vibratorService.vibrate(1000)

        Handler().postDelayed({
            finish()
        }, 6000)
    }
}
