package pt2.lunata.pscdeliserdang

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Environment
import android.text.Editable
import android.text.SpannableString
import android.text.Spanned
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Base64
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexboxLayout
import com.rilixtech.materialfancybutton.MaterialFancyButton
import khronos.toString
import org.jetbrains.anko.custom.ankoView
import pt2.lunata.pscdeliserdang.component.SquareImageView
import pt2.lunata.pscdeliserdang.component.ZoomableImageView
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.File.separator
import java.io.FileOutputStream
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}


fun TextView.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}

inline fun <T> MutableList<T>.mapInPlace(mutator: (T) -> T) {
    val iterate = this.listIterator()
    while (iterate.hasNext()) {
        val oldValue = iterate.next()
        val newValue = mutator(oldValue)
        if (newValue !== oldValue) {
            iterate.set(newValue)
        }
    }
}

fun textViewMakeLinks(textView: TextView, links: Array<String>, clickableSpans: Array<ClickableSpan>) {
    val spannableString = SpannableString(textView.text)
    for (i in links.indices) {
        val clickableSpan = clickableSpans[i]
        val link = links[i]

        val startIndexOfLink = textView.text.toString().indexOf(link)
        spannableString.setSpan(
                clickableSpan, startIndexOfLink,
                startIndexOfLink + link.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
    textView.highlightColor = Color.TRANSPARENT // prevent TextView change background when highlight
    textView.movementMethod = LinkMovementMethod.getInstance()
    textView.setText(spannableString, TextView.BufferType.SPANNABLE)
}

//inline fun ViewManager.cardView(init: CardView.() -> Unit): CardView {
//    return ankoView({ CardView(it) }, theme = 0, init = init)
//}

inline fun ViewManager.recyclerView(init: RecyclerView.() -> Unit): RecyclerView {
    return ankoView({ RecyclerView(it) }, theme = 0, init = init)
}

inline fun ViewManager.zoomableImageView(init: ZoomableImageView.() -> Unit): ZoomableImageView {
    return ankoView({ ZoomableImageView(it) }, theme = 0, init = init)
}

inline fun ViewManager.flexboxLayout(init: FlexboxLayout.() -> Unit): FlexboxLayout {
    return ankoView({ FlexboxLayout(it) }, theme = 0, init = init)
}

inline fun ViewManager.materialFancyButton(init: MaterialFancyButton.() -> Unit): MaterialFancyButton {
    return ankoView({ MaterialFancyButton(it) }, theme = 0, init = init)
}

inline fun ViewManager.squareImageView(init: SquareImageView.() -> Unit): SquareImageView {
    return ankoView({ SquareImageView(it) }, theme = 0, init = init)
}

// manage fragment https://medium.com/thoughts-overflow/how-to-add-a-fragment-in-kotlin-way-73203c5a450b
inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction().func().commit()
}

fun AppCompatActivity.addFragment(fragment: Fragment, frameId: Int) {
    supportFragmentManager.inTransaction { add(frameId, fragment) }
}

fun AppCompatActivity.replaceFragment(fragment: Fragment, frameId: Int) {
    supportFragmentManager.inTransaction {
        setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
        replace(frameId, fragment)
    }
}

// https://stackoverflow.com/a/51768312/4917020
private fun View.addRipple() = with(TypedValue()) {
    context.theme.resolveAttribute(android.R.attr.selectableItemBackground, this, true)
    setBackgroundResource(resourceId)
}

private fun View.addCircleRipple() = with(TypedValue()) {
    context.theme.resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, this, true)
    setBackgroundResource(resourceId)
}

// indo date format
fun Date.toStringIndo(format: String): String {
    return SimpleDateFormat(format, Locale("in", "ID")).format(this)
}

/**------------------------------------------
 *
 *        fungsi yang sering digunakan
 *
 * -------------------------------------------*/

fun getThemeColor(context: Context, attr: Int): Int {
    val typedValue = TypedValue()
    context.theme.resolveAttribute(attr, typedValue, true)
    return typedValue.data
}

fun getColor(context: Context, colorResource: Int): Int {
    return ContextCompat.getColor(context, colorResource)
}

fun getDrawable(context: Context, colorResource: Int): Drawable? {
    return ContextCompat.getDrawable(context, colorResource)
}

fun fileIsExistOnDownloadFolder(context: Context, name: String): Boolean {
    val file = File(App.DOCUMENT_DOWNLOAD_FOLDER + name)
    return file.exists()
}

fun fileNameFromURL(url: String): String {
    try {
        val resource = URL(url)
        val host = resource.host
        if (host.isNotEmpty() && url.endsWith(host)) {
            // handle ...example.com
            return ""
        }
    } catch (e: MalformedURLException) {
        return ""
    }

    val startIndex = url.lastIndexOf('/') + 1
    val length = url.length

    // find end index for ?
    var lastQMPos = url.lastIndexOf('?')
    if (lastQMPos == -1) {
        lastQMPos = length
    }

    // find end index for #
    var lastHashPos = url.lastIndexOf('#')
    if (lastHashPos == -1) {
        lastHashPos = length
    }

    // calculate the end index
    val endIndex = Math.min(lastQMPos, lastHashPos)
    return url.substring(startIndex, endIndex)
}

fun base64ToBitmap(base64: String): Bitmap {
    val decodedBase64 = Base64.decode(base64, android.util.Base64.DEFAULT)
    return BitmapFactory.decodeByteArray(decodedBase64, 0, decodedBase64.size)
}

fun bitmapToBase64(bitmap: Bitmap): String {
    val byteArrayOutputStream = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
    val byteArray = byteArrayOutputStream.toByteArray()
    return Base64.encodeToString(byteArray, Base64.DEFAULT)
}

fun savePhotoToDeviceStorageGetPath(bitmap: Bitmap): String {
    val rootPicturesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString()
    val photoDir = File(rootPicturesDir + separator + "psc")
    if (!photoDir.exists()) photoDir.mkdirs()

    val today = Calendar.getInstance().time
    val dateFormat = today.toString("yyyy-MM-dd_hh_mm_ss")
    val fileName = "foto_$dateFormat.png"
    val fileImage = File(photoDir, fileName)

    val fileOutputStream = FileOutputStream(fileImage)
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)
    fileOutputStream.flush()
    fileOutputStream.close()

    return fileImage.path
}


// Extension method to resize bitmap to maximum width and height
fun Bitmap.scale(maxWidthAndHeight: Int): Bitmap {
    var newWidth: Int
    var newHeight: Int

    if (this.width >= this.height) {
        val ratio: Float = this.width.toFloat() / this.height.toFloat()

        newWidth = maxWidthAndHeight
        // Calculate the new height for the scaled bitmap
        newHeight = Math.round(maxWidthAndHeight / ratio)
    } else {
        val ratio: Float = this.height.toFloat() / this.width.toFloat()

        // Calculate the new width for the scaled bitmap
        newWidth = Math.round(maxWidthAndHeight / ratio)
        newHeight = maxWidthAndHeight
    }

    return Bitmap.createScaledBitmap(
            this,
            newWidth,
            newHeight,
            false
    )
}




