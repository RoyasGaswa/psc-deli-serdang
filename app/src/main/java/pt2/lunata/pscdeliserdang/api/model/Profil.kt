package pt2.lunata.pscdeliserdang.api.model

import com.google.gson.annotations.SerializedName

object Profil {
    data class DetailRespon(val data: Detail, val message: String)
    data class Detail(
            val nama: String,
            val email: String,
            val tanggalLahir: String,
            val ktp: String,
            val noHp: String,
            val alamat: String,
            val bpjs: String,
            val kelamin: String,
            val goldar: String,
            val statusUser: String,
            val foto: String,
            val fotoUrl: String,
            val kecamatan: String,
            val idKecamatan: String,
            val kelurahan: String,
            val idKelurahan: String
    )

    data class DetailResponUser(val data: DetailUser, val message: String)
    data class DetailUser(
        val name: String,
        val email: String,
        val ktp: String,
        val address:String,
        val phone: String,
        val approved: String?=null,
        val selfie: String?=null,
        @field:SerializedName("selfie-ktp")
        val selfieKtp: String?=null,
        val subdistrict_area: String,
        val idKecamatan: String,
        val village_area: String,
        val idKelurahan: String,
        val deleted_at:String?=null,
        val gender:String,
        val age:String,
        @field:SerializedName("sub-district-id")
        val subdistrict_id: String,
        @field:SerializedName("statement_of_rejection")
        val rejection:String?
    )

    data class MessageResponse(val message: String)

    data class StatusVerifikasiResponse(val idStatus: Int, val message: String)
}
