package pt2.lunata.publicsafetycenter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


class BasePagerAdapter(fm: FragmentManager, val pages: List<Pair<String, Fragment>>) : FragmentPagerAdapter(fm) {

    override fun getCount(): Int = pages.size


    override fun getPageTitle(position: Int): CharSequence? {
        return pages[position].first
    }

    override fun getItem(position: Int): Fragment {
        return pages[position].second
    }
}