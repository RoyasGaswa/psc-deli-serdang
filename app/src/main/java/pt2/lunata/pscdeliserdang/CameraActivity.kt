package pt2.lunata.pscdeliserdang

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.SurfaceTexture
import android.graphics.drawable.BitmapDrawable
import android.hardware.camera2.*
import android.os.*
/*import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity*/
import android.util.Log
import android.view.Surface
import android.view.TextureView
import android.view.View
import kotlinx.android.synthetic.main.activity_camera.*
import org.jetbrains.anko.*
import java.lang.IllegalArgumentException
import java.util.*
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import pt2.lunata.pscdeliserdang.helper.FilePathHelper


class CameraActivity : AppCompatActivity() {
    companion object {
        val storageCameraPermission = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE)
        const val REQUEST_PERMISSION_CAMERA_STORAGE = 100
        const val RC_IMAGE_PICKER = 11

        private val TAG = CameraActivity::class.qualifiedName
        fun newInstance() = CameraActivity()
    }

    private val MAX_PREVIEW_WIDTH = 1280
    private val MAX_PREVIEW_HEIGHT = 720

    private lateinit var cameraDevice: CameraDevice
    private lateinit var captureSession: CameraCaptureSession
    private lateinit var captureRequestBuilder: CaptureRequest.Builder

    private lateinit var backgroundThread: HandlerThread
    private lateinit var backgroundHandler: Handler

    private var isConfirmed: Boolean = true
    private var isGalleryPickerEnabled: Boolean = true
    private var isFrontCameraFirst: Boolean = true


    val deviceStateCallback = object : CameraDevice.StateCallback() {
        override fun onOpened(camera: CameraDevice) {
            cameraDevice = camera
            previewSession()
        }

        override fun onDisconnected(camera: CameraDevice) {
            camera.close()
        }

        override fun onError(camera: CameraDevice, error: Int) {
            longToast("camera error code: $error")
            this@CameraActivity.finish()
        }
    }

    val cameraManager by lazy {
        this@CameraActivity.getSystemService(Context.CAMERA_SERVICE) as CameraManager
    }

    private fun previewSession() {
        val surfaceTexture = textureView.surfaceTexture
        surfaceTexture.setDefaultBufferSize(MAX_PREVIEW_WIDTH, MAX_PREVIEW_HEIGHT)
        val surface = Surface(surfaceTexture)

        captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
        captureRequestBuilder.addTarget(surface)

        cameraDevice.createCaptureSession(
                Arrays.asList(surface),
                object : CameraCaptureSession.StateCallback() {
                    override fun onConfigureFailed(session: CameraCaptureSession) {
                        longToast("Capture Failed")
                        Log.e(TAG, session.toString())
                    }

                    override fun onConfigured(session: CameraCaptureSession) {
                        captureSession = session
                        captureRequestBuilder.set(
                                CaptureRequest.CONTROL_AF_MODE,
                                CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE
                        )
                        captureSession.setRepeatingRequest(captureRequestBuilder.build(), null, null)
                    }
                }, null
        )
    }

    private fun closeCamera() {
        if (this::captureSession.isInitialized) captureSession.close()
        if (this::cameraDevice.isInitialized) cameraDevice.close()
    }

    private fun startBackgroundThread() {
        backgroundThread = HandlerThread("Camera").also { it.start() }
        backgroundHandler = Handler(backgroundThread.looper)
    }

    private fun stopBackgroundThread() {
        backgroundThread.quitSafely()
        try {
            backgroundThread.join()
        } catch (e: InterruptedException) {
            Log.e(TAG, e.toString())
        }
    }

    private fun <T> cameraCharacteristics(cameraId: String, key: CameraCharacteristics.Key<T>): T {
        val characteristics = cameraManager.getCameraCharacteristics(cameraId)
        return when (key) {
            CameraCharacteristics.LENS_FACING -> characteristics.get(key)!!
            CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP -> characteristics.get(key)!!
            else -> throw IllegalArgumentException("Key not recognized")

        }
    }

    private fun cameraId(lens: Int): String {
        var deviceId = listOf<String>()
        try {
            val cameraIdList = cameraManager.cameraIdList
            deviceId = cameraIdList.filter { lens == cameraCharacteristics(it, CameraCharacteristics.LENS_FACING) }
        } catch (e: CameraAccessException) {
            Log.e(TAG, e.toString())
        }
        return deviceId[0]
    }

    @SuppressLint("MissingPermission")
    private fun connectCamera() {
        val lens = if (isFrontCameraFirst) CameraCharacteristics.LENS_FACING_FRONT else CameraCharacteristics.LENS_FACING_BACK
        val deviceId = cameraId(lens)
        Log.d(TAG, "device id: $deviceId")

        try {
            cameraManager.openCamera(deviceId, deviceStateCallback, backgroundHandler)
        } catch (e: CameraAccessException) {
            Log.e(TAG, e.toString())
        } catch (e: InterruptedException) {
            Log.e(TAG, "Open Camera device interrupted while opened")
        }
    }

    private val surfaceListener = object : TextureView.SurfaceTextureListener {
        override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture?, width: Int, height: Int) {
        }

        override fun onSurfaceTextureUpdated(surface: SurfaceTexture?) = Unit

        override fun onSurfaceTextureDestroyed(surface: SurfaceTexture?): Boolean = true

        override fun onSurfaceTextureAvailable(surface: SurfaceTexture?, width: Int, height: Int) {
            Log.d(TAG, "texture surface width:$width heigth:$height")
            openCamera()
        }

    }

    private fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
        if (context != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }
        return true
    }

    override fun onResume() {
        super.onResume()

        startBackgroundThread()
        if (textureView.isAvailable)
            openCamera()
        else
            textureView.surfaceTextureListener = surfaceListener
    }

    override fun onPause() {
        closeCamera()
        stopBackgroundThread()

        super.onPause()
    }

    private fun openCamera() {
        if (!hasPermissions(this, *storageCameraPermission)) {
            ActivityCompat.requestPermissions(this, storageCameraPermission, REQUEST_PERMISSION_CAMERA_STORAGE)
        } else {
            connectCamera()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)

        isConfirmed = intent.getBooleanExtra("confirmation", true)
        isGalleryPickerEnabled = intent.getBooleanExtra("gallery-picker", true)
        isFrontCameraFirst = intent.getBooleanExtra("front-camera", false)

        textureView.visibility = View.VISIBLE
        btnCloseCamera.visibility = View.VISIBLE

        fabTakePhoto.setOnClickListener {
            hideShootControl()
            imagePreview.imageBitmap = textureView.bitmap

            closeCamera()
            stopBackgroundThread()
        }

        fabChoosePhoto.setOnClickListener {
            val photoPickerIntent = Intent(Intent.ACTION_PICK)
            photoPickerIntent.type = "image/*"
            startActivityForResult(photoPickerIntent, RC_IMAGE_PICKER)
        }

        fabFlipCamera.setOnClickListener {
            isFrontCameraFirst = !isFrontCameraFirst

            closeCamera()
            stopBackgroundThread()

            startBackgroundThread()
            openCamera()

            val iconDrawable = if (isFrontCameraFirst) getDrawable(
                this@CameraActivity,
                R.drawable.ic_camera_rear_black_24dp
            )
            else getDrawable(this@CameraActivity, R.drawable.ic_camera_front_black_24dp)

            fabFlipCamera.setImageDrawable(iconDrawable)
        }

        btnCloseCamera.setOnClickListener { _ ->
            finish()
        }

        btnCancel.setOnClickListener { _ ->
            showShootControl()

            startBackgroundThread()
            openCamera()
        }

        btnSimpan.setOnClickListener { _ ->
            if (isConfirmed) {
                alert {
                    title = "Pakai foto ini?"

                    positiveButton("Ya") {
                        returnToActivity()
                    }

                    negativeButton("Cancel") {}
                }.show()
            } else {
                returnToActivity()
            }
        }
        if (isGalleryPickerEnabled) fabChoosePhoto.show() else fabChoosePhoto.hide()
    }

    private fun returnToActivity() {
        val bitmap = (imagePreview.drawable as BitmapDrawable).bitmap
        val imagePath = savePhotoToDeviceStorageGetPath(bitmap)

        intent.putExtra("foto-path", imagePath)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            Activity.RESULT_OK -> {
                if (requestCode == RC_IMAGE_PICKER) {
                    val path = FilePathHelper.getPath(this@CameraActivity, data?.data)

                    intent.putExtra("foto-path", path)
                    intent.putExtra("foto",data?.data)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            }
            Activity.RESULT_CANCELED -> longToast("Ada masalah image picker")
        }
    }

    private fun hideShootControl() {
        fabChoosePhoto.hide()
        fabTakePhoto.hide()
        fabFlipCamera.hide()
        layoutMessage.visibility = View.VISIBLE
        imagePreview.visibility = View.VISIBLE
        textureView.visibility = View.GONE
    }

    private fun showShootControl() {
        if (isGalleryPickerEnabled) fabChoosePhoto.show() else fabChoosePhoto.hide()
        fabTakePhoto.show()
        fabFlipCamera.show()
        layoutMessage.visibility = View.GONE
        imagePreview.visibility = View.GONE
        textureView.visibility = View.VISIBLE
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
