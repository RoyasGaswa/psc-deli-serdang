package pt2.lunata.pscdeliserdang

import android.app.Application
import android.os.Environment
import android.util.Log
import com.facebook.stetho.Stetho
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.GsonBuilder
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.OkHttpClient
import pt2.lunata.pscdeliserdang.helper.SharedPreferencesHelper
import java.io.File.separator
import java.util.concurrent.TimeUnit

class App : Application() {
    companion object {
        const val RAPAT_ALARM_NOTIFICATION_ID = 11

        const val API = "https://psc-deliserdang.lunata.co.id/api/"
        const val NOTIFICATION_CHANNEL_INFO = "psc_medis"
        const val DEFAULT_DATE_FORMAT = "dd MMM yyyy"
        const val DEFAULT_TIME_FORMAT = "HH:mm:ss"
        const val DEFAULT_DATABASE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS"
        const val FOTO_MAX_WIDTH_OR_HEIGHT = 800

        // broadcast untuk finish activity lain
        const val BC_LANDING = "finish_landing_activity"
        const val BC_REGISTER = "finish_register_activity"
        const val BC_FOTOKTP = "finish_ktp_activity"
        const val BC_VERIFIKASI = "finish_verifikasi_activity"
        const val BC_SELESAI = "finish_selesai_activity"

        const val RI_MAP_ORIGIN = "map_origin"

        lateinit var DOCUMENT_DOWNLOAD_FOLDER: String
            private set // cuman 'App' yang bisa set value

        lateinit var DEVICE_TOKEN: String
            private set

        lateinit var httpClientBuilder: OkHttpClient.Builder
            private set

        val GSON = GsonBuilder()
            .setDateFormat("yyyy-MM-dd")
            .serializeNulls()
            .setLenient()
            .create()
    }

    // dari ligat

    override fun onCreate() {
        super.onCreate()
        SharedPreferencesHelper.init(applicationContext)

        // jangan lupa edit lokasi tujuan download file di "DosirFilesAdapter.downloadFile()"
        DOCUMENT_DOWNLOAD_FOLDER = applicationContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)!!.path +
                separator

        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
            if (it.isSuccessful) {
                DEVICE_TOKEN = it.result!!.token
                Log.i("Device Token", DEVICE_TOKEN)
            } else Log.e("DEVICE TOKEN", "Error ngambil device token")
        }

        httpClientBuilder = OkHttpClient.Builder()
            .connectTimeout(2, TimeUnit.MINUTES)
            .readTimeout(2, TimeUnit.MINUTES)
            .writeTimeout(2, TimeUnit.MINUTES)

        if (BuildConfig.DEBUG) {
            val chuckInterceptor = ChuckInterceptor(applicationContext)
            Stetho.initializeWithDefaults(this@App)
            httpClientBuilder.addInterceptor(chuckInterceptor)
        }

    }

}