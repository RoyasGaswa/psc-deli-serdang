package pt2.lunata.pscdeliserdang.main

import android.content.Intent
import android.net.Uri
import androidx.core.content.ContextCompat.startActivity
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.layanan_item.view.*
import pt2.lunata.pscdeliserdang.R
import pt2.lunata.pscdeliserdang.api.model.Layanan
import pt2.lunata.pscdeliserdang.inflate
import java.util.*

class LayananAdapter(val fullList: List<Layanan.Layanan>? = null, val listener: ((Layanan.Layanan) -> Unit)? = null) : RecyclerView.Adapter<LayananAdapter.ViewHolder>() {
    private val placeholder = fullList == null
    private var mainList = fullList?.toList() ?: listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.layanan_item))
    }

    override fun getItemCount() = if (placeholder) 9 else mainList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (!placeholder) holder.bind(mainList[position], listener)
    }

    fun customFilter(param: (List<Layanan.Layanan>) -> List<Layanan.Layanan>) {
        mainList = param(fullList!!)
        notifyDataSetChanged()
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Layanan.Layanan, listener: ((Layanan.Layanan) -> Unit)?) = with(itemView) {
            textLayanan.text = item.nama
            textAlamat.text = item.alamat
            textTelp.text = item.noHp

            val bpjs = when (item.statusBpjs) {
                "Y" -> "Menerima BPJS"
                else -> "Tidak menerima BPJS"
            }

            textStatusBpjs.text = bpjs
            textTipeLayanan.text = item.layananTipe

            layoutAlamat.setOnClickListener {
                val latLong = item.koordinat.replace("\\s".toRegex(), "").split(",").map {
                    it.toDouble()
                }
                val uriMaps = String.format(Locale.ENGLISH, "geo:0,0?q=%f,%f(%s)", latLong[0], latLong[1], item.alamat)
                val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse(uriMaps))
                startActivity(context, mapIntent, null)
            }

            layoutTelp.setOnClickListener {
                val callIntent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + item.noHp))
                startActivity(context, callIntent, null)
            }
        }
    }
}