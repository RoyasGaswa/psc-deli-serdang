package pt2.lunata.pscdeliserdang.component

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.text.InputFilter
import android.text.InputType
import android.util.AttributeSet
import android.util.Patterns
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import khronos.Dates
import khronos.toDate
import org.jetbrains.anko.dip
import org.jetbrains.anko.lines
import pt2.lunata.pscdeliserdang.App
import pt2.lunata.pscdeliserdang.R
import pt2.lunata.pscdeliserdang.afterTextChanged

import java.text.DateFormatSymbols

@SuppressLint("SetTextI18n")
class FormBoxieEditText(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

    var textNullable: String?
        get() = if (inputText.text != null && inputText.text!!.isEmpty()) null else inputText.text.toString()
        set(value) {
            inputText.setText(value)
        }

    var text: String
        get() = inputText.text.toString()
        set(value) {
            inputText.setText(value)
        }

    val inputText: TextInputEditText
    val layoutContainer: FrameLayout
    val layoutInputText: TextInputLayout
    var icon: ImageView

    var datePickerDialog: DatePickerDialog? = null
    var year: Int
    var month: Int
    var day: Int
    var value: String? = null

    private val isTextEnabled: Boolean
    private val isRequired: Boolean
    private val isWrapText: Boolean
    private val isTextFocusable: Boolean
    private val isDatePicker: Boolean
    private val inputType: Int
    private val maxLength: Int

    init {
        inflate(context, R.layout.component_form_boxie_edit_text, this)

        inputText = findViewById(R.id.inputText)
        layoutInputText = findViewById(R.id.layoutText)
        layoutContainer = findViewById(R.id.layoutContainer)
        icon = findViewById(R.id.imgDropdown)

        @SuppressLint("CustomViewStyleable")
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.ComponentFormMaterialEditText)
        layoutInputText.hint = attributes.getString(R.styleable.ComponentFormMaterialEditText_hint)
        textNullable = attributes.getString(R.styleable.ComponentFormMaterialEditText_android_text)
        isTextFocusable = attributes.getBoolean(R.styleable.ComponentFormMaterialEditText_focusable, true)
        isTextEnabled = attributes.getBoolean(R.styleable.ComponentFormMaterialEditText_enabled, true)
        isWrapText = attributes.getBoolean(R.styleable.ComponentFormMaterialEditText_wrapText, false)
        isRequired = attributes.getBoolean(R.styleable.ComponentFormMaterialEditText_required, false)
        inputType = attributes.getInt(
                R.styleable.ComponentFormMaterialEditText_android_inputType,
                InputType.TYPE_CLASS_TEXT
        )
        maxLength = attributes.getInt(
                R.styleable.ComponentFormMaterialEditText_android_maxLength,
                9999999
        )

        inputText.filters = arrayOf(InputFilter.LengthFilter(maxLength))
        inputText.inputType = inputType

        isDatePicker = attributes.getBoolean(R.styleable.ComponentFormMaterialEditText_datePicker, false)
        year = attributes.getInt(R.styleable.ComponentFormMaterialEditText_year, 2000)
        month = attributes.getInt(R.styleable.ComponentFormMaterialEditText_month, 1)
        day = attributes.getInt(R.styleable.ComponentFormMaterialEditText_day, 1)

        inputText.apply {
            isFocusable = isTextFocusable && !isDatePicker
            if (!isFocusable) {
                icon.visibility = View.VISIBLE
            }
        }

        if (icon.visibility == View.VISIBLE) {
            inputText.right = dip(16)
        }

        inputText.apply {
            isEnabled = isTextEnabled
        }

        if (isRequired) {
            inputText.afterTextChanged {
                if (inputText.text != null && inputText.text!!.isEmpty()) inputText.error = "Input dibutuhkan"
                else inputText.error = null
            }
        }

        if (inputType == InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS + 1) {  // input type email ditambah 1 gara gara bitvise https://stackoverflow.com/a/47186831/4917020
            inputText.setOnFocusChangeListener { _, hasFocus ->
                if (!hasFocus) {
                    inputText.error =
                            if (Patterns.EMAIL_ADDRESS.matcher(inputText.text.toString()).matches()) null else "Invalid email"
                }
            }
        }

        if (isWrapText) {
            inputText.apply {
                lines = 3
                gravity = Gravity.TOP
                inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE or InputType.TYPE_CLASS_TEXT or
                        InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
            }
        }

        if (isDatePicker) {
            datePickerDialog =
                    DatePickerDialog(context, DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                        inputText.setText("$dayOfMonth ${DateFormatSymbols().shortMonths[monthOfYear]} $year")
                        inputText.text?.toString()?.toDate(App.DEFAULT_DATE_FORMAT)
                    }, year, month, day)
            datePickerDialog?.datePicker?.minDate = Dates.of(1930, 1, 1).time
        }

        attributes.recycle()
    }

    private fun validateRequired(): Boolean {
        return if (isRequired) {
            if (inputText.text != null && inputText.text!!.isEmpty()) {
                inputText.error = "Input Dibutuhkan"
                false
            } else true
        } else true
    }

    fun isValid(): Boolean = inputText.error == null && validateRequired()
}