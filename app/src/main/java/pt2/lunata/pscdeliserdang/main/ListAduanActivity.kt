package pt2.lunata.pscdeliserdang.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_list_aduan.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.backgroundColor
import pt2.lunata.pscdeliserdang.R
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.textColor
import pt2.lunata.pscdeliserdang.BaseActivity
import pt2.lunata.pscdeliserdang.api.AduanService
import pt2.lunata.pscdeliserdang.getColor

class ListAduanActivity : BaseActivity() {

    val compositeDisposable = CompositeDisposable()

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_aduan)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbarTitle.text = "List Aduan"
        toolbarTitle.textColor = getColor(this, R.color.pscAccent)
        toolbarTitle.visibility = View.VISIBLE
        toolbar.backgroundColor = getColor(this, android.R.color.transparent)
        appBar.backgroundColor = getColor(this, android.R.color.transparent)

        appBar.outlineProvider = null

        swipeRefreshLayout.setOnRefreshListener { setupView() }
        setupView()
    }

    private fun setupView() {
        imgHistoryEmpty.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
        recyclerView.layoutManager = LinearLayoutManager(this)
        swipeRefreshLayout.isRefreshing = true

        val historyDisposable = AduanService.create()
            .history("Bearer "+userPref.token!!,userPref.uid!!,"ongoing")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    val data = result.data
                    recyclerView.adapter = ListAduanAdapter(data) { aduan ->
                        startActivity<DetailAduanActivity>("extra_data" to aduan)
                    }

                    swipeRefreshLayout.isRefreshing = false
                },
                { throwable ->
                    swipeRefreshLayout.isRefreshing = false
                    imgHistoryEmpty.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                }
            )

        compositeDisposable.add(historyDisposable)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_history_aduan, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.nav_history_aduan -> {
                startActivity<ListHistoryAduanActivity>()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
