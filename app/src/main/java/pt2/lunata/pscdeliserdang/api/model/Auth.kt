package pt2.lunata.pscdeliserdang.api.model

import ir.mirrajabi.searchdialog.core.Searchable

object Auth {
    data class UserResponse(val data: User, val message: String)
    data class User(
        val uid: String,
        val token:String
    )

    data class MessageResponse(val message: String)

    data class CekHp(val jumlah: Int, val message: String)

    data class Uid(val uid: String, val message: String)

    data class ListItemResponse(val data: List<Item>, val message: String)
    data class Item(val nama: String, val uid: String)

    data class ListKecamatanResponse(val data: List<KecamatanItem>, val message: String)
    data class KecamatanItem(val name: String, val id: String)

    data class KecamatanSearchableItem(val name: String, val id: String) : Searchable {
        override fun getTitle(): String = name
    }
}
