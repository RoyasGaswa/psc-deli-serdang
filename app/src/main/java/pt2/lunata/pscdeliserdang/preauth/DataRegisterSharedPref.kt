package pt2.lunata.pscdeliserdang.preauth

import pt2.lunata.pscdeliserdang.helper.SharedPreferencesHelper

class DataRegisterSharedPref : SharedPreferencesHelper() {
    var uid by stringPref()
    var nik by stringPref()
    var nama by stringPref()
    var email by stringPref()
    var pass by stringPref()
    //var bpjs by stringPref()
    //var ktp by stringPref()
    var tglLahir by stringPref()
    var kelamin by stringPref()
    //var goldar by stringPref()
    var noHp by stringPref()
    var alamat by stringPref()
    var idKecamatan by stringPref()
    var idKelurahan by stringPref()
    var fotoKtpPath by stringPref()
    var fotoUserPath by stringPref()

    fun clear() {
        uid = null
        nama = null
        email = null
        pass = null
        //bpjs = null
        //ktp = null
        tglLahir = null
        kelamin = null
        //goldar = null
        noHp = null
        alamat = null
        idKecamatan = null
        idKelurahan = null
        fotoKtpPath = null
        fotoUserPath = null
    }
}