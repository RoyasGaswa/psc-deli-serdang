package pt2.lunata.pscdeliserdang

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.PersistableBundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import org.jetbrains.anko.startActivity
import pt2.lunata.pscdeliserdang.preauth.LandingActivity

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {
    companion object {
        val TAG = BaseActivity::class.java.simpleName
    }

    val userPref = UserSharedPref()

    private fun forceLogin() {
        if (!userPref.isLoggedIn()) {
            startActivity<LandingActivity>()
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        forceLogin()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}