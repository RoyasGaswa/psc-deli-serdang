package pt2.lunata.pscdeliserdang

import pt2.lunata.pscdeliserdang.helper.SharedPreferencesHelper

class UserSharedPref : SharedPreferencesHelper() {
    var token by stringPref()
    var uid by stringPref()
    var nama by stringPref()
    var alamat by stringPref()
    var ktp by stringPref()
    var uidKelamin by stringPref()
    var hp by stringPref()
    var tglLahir by stringPref()
    var umur by stringPref()
    var fotoUrl by stringPref()
    var kecamatan by stringPref()
    var kelurahan by stringPref()
    var idStatus by stringPref()
    var lokasiSekarang by stringPref()
    var koordinatSekarang by stringPref()
    var deleted_at by stringPref()
    var idKecamatan by stringPref()
    fun isLoggedIn(): Boolean = uid != null

    fun logout() {
        uid = null
        nama = null
        alamat=null
        ktp = null
        uidKelamin = null
        hp = null
        tglLahir = null
        fotoUrl = null
        kecamatan=null
        kelurahan=null
        idStatus = null
        lokasiSekarang = null
        koordinatSekarang = null
        deleted_at=null
        idKecamatan=null
    }
}