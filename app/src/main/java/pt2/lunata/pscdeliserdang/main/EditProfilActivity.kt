package pt2.lunata.pscdeliserdang.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat
import kotlinx.android.synthetic.main.activity_edit_profil.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk27.coroutines.onFocusChange
import pt2.lunata.pscdeliserdang.BaseActivity
import pt2.lunata.pscdeliserdang.R
import pt2.lunata.pscdeliserdang.api.AuthService
import pt2.lunata.pscdeliserdang.api.ProfilService
import pt2.lunata.pscdeliserdang.api.model.Auth
import pt2.lunata.pscdeliserdang.getColor

class EditProfilActivity : BaseActivity() {

    val compositeDisposable = CompositeDisposable()
    val TAG = this::class.java.simpleName

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profil)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbarTitle.text = "Edit Profil"
        toolbarTitle.textColor = getColor(this@EditProfilActivity, R.color.pscAccent)
        toolbarTitle.visibility = View.VISIBLE
        toolbar.backgroundColor = getColor(this, android.R.color.transparent)

        appBar.outlineProvider = null
        appBar.backgroundColor = getColor(this, android.R.color.transparent)

        setupForm()
    }

    private fun setupForm() {
        val detailDisposable = ProfilService.create()
                .detail(userPref.uid!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            val data = result.data

                            inputEmail.text = data.email
                            inputHp.text = data.noHp
                            //inputNoBpjs.text = data.bpjs
                            inputAlamat.text = data.alamat
                            inputKecamatan.text = data.kecamatan
                            inputKecamatan.value = data.idKecamatan
                            inputKelurahan.text = data.kelurahan
                            inputKelurahan.value = data.idKelurahan

                            cekHp()
                            setupKecamatanKelurahan()
                            //setupBtnSubmit()
                        },
                        { throwable ->
                            Log.e(TAG, throwable.message)
                            toast("Ada masalah")
                        }
                )
        compositeDisposable.add(detailDisposable)
    }

    private fun setupKecamatanKelurahan() {
        val onClickListenerKecamatan = View.OnClickListener { _ ->
            inputKecamatan.inputText.isEnabled = false
            inputKecamatan.icon.isEnabled = false
            val kecamatanDisposable =
                    AuthService.create()
                            .kecamatan()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    { result ->
                                        val dataKecamatan =
                                                result.data.map { Auth.KecamatanSearchableItem(it.name, it.id) }.sortedBy { it.name }

                                        SimpleSearchDialogCompat(
                                                this@EditProfilActivity, "Kecamatan", "Cari..."
                                                , null, ArrayList(dataKecamatan)
                                        ) { dialog, item, _ ->
                                            inputKecamatan.text = item.name
                                            inputKecamatan.value = item.id
                                            dialog.hide()
                                        }.show()

                                        inputKecamatan.inputText.isEnabled = true
                                        inputKecamatan.icon.isEnabled = true
                                    },
                                    { throwable ->
                                        inputKecamatan.inputText.isEnabled = true
                                        inputKecamatan.icon.isEnabled = true
                                        Log.e(TAG, throwable.message)
                                        longToast("Mohon Cek Koneksi Anda")
                                    }
                            )

            compositeDisposable.add(kecamatanDisposable)
        }

        inputKecamatan.inputText.setOnClickListener(onClickListenerKecamatan)
        inputKecamatan.icon.setOnClickListener(onClickListenerKecamatan)


        val onClickListenerKelurahan = View.OnClickListener { _ ->
            if (inputKecamatan.value != null) {
                inputKelurahan.inputText.isEnabled = false
                inputKelurahan.icon.isEnabled = false
                val kelurahanDisposable =
                        AuthService.create()
                                .kelurahan(inputKecamatan.value!!)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        { result ->
                                            val dataKelurahan =
                                                    result.data.map { Auth.KecamatanSearchableItem(it.name, it.id) }
                                                            .sortedBy { it.name }

                                            SimpleSearchDialogCompat(
                                                    this@EditProfilActivity, "Kelurahan", "Cari..."
                                                    , null, ArrayList(dataKelurahan)
                                            ) { dialog, item, _ ->
                                                inputKelurahan.text = item.name
                                                inputKelurahan.value = item.id
                                                dialog.hide()
                                            }.show()

                                            inputKelurahan.inputText.isEnabled = true
                                            inputKelurahan.icon.isEnabled = true
                                        },
                                        { throwable ->
                                            inputKelurahan.inputText.isEnabled = true
                                            inputKelurahan.icon.isEnabled = true
                                            Log.e(TAG, throwable.message)
                                            longToast("Mohon Cek Koneksi Anda")
                                        }
                                )

                compositeDisposable.add(kelurahanDisposable)
            } else {
                toast("Mohon pilih kecamatan terlebih dahulu")
            }
        }

        inputKelurahan.inputText.setOnClickListener(onClickListenerKelurahan)
        inputKelurahan.icon.setOnClickListener(onClickListenerKelurahan)

    }

    private fun cekHp() {
        inputHp.inputText.onFocusChange { _, hasFocus ->
            if (!hasFocus) {
                val cekHpDisposable = AuthService.create()
                        .cekNoHp(inputHp.text)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { result ->
                                    if (result.jumlah > 0) {
                                        inputHp.inputText.error = "Nomor Hp sudah terdaftar"
                                        alert {
                                            message = "Nomor HP sudah terdaftar. Mohon input nomor lain."
                                            negativeButton("Tutup") {}
                                        }.show()
                                    } else {
                                        inputHp.inputText.error = null
                                    }
                                },
                                {
                                }
                        )
                compositeDisposable.add(cekHpDisposable)
            }
        }
    }

   /* private fun setupBtnSubmit() {
        btnKirim.setOnClickListener {
            btnKirim.isEnabled = false
            if (validateForm()) {
                val email = inputEmail.text
                val bpjs = inputNoBpjs.text
                val hp = inputHp.text
                val alamat = inputAlamat.text
                val idKecamatan = inputKecamatan.value!!
                val idKelurahan = inputKelurahan.value!!

                val registerDisposable =
                        ProfilService.create()
                                .edit(
                                        userPref.uid!!,
                                        email,
                                        bpjs,
                                        hp,
                                        alamat,
                                        idKecamatan,
                                        idKelurahan
                                )
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        { result ->
                                            toast("Edit Berhasil")
                                            finish()
                                        },
                                        { t ->
                                            btnKirim.isEnabled = true
                                            Log.e(TAG, t.message)
                                            longToast("Mohon cek koneksi anda")
                                        }
                                )
                compositeDisposable.add(registerDisposable)
            } else {
                btnKirim.isEnabled = true
                longToast("Harap isi semua data")
            }
        }
    }

    private fun validateForm() =
            inputEmail.isValid() && inputHp.isValid()
                    && inputAlamat.isValid() && inputNoBpjs.isValid()
                    && inputKecamatan.isValid() && inputKelurahan.isValid()*/
}
