package pt2.lunata.pscdeliserdang.api

import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pt2.lunata.pscdeliserdang.api.model.Auth
import retrofit2.http.*

interface AuthService {
    @FormUrlEncoded
    @POST("login")
    fun login(
            @Field("phone") username: String,
            @Field("password") password: String,
            @Field("device_id") deviceId: String
    ): Observable<Auth.UserResponse>

    @FormUrlEncoded
    @POST("logout")
    fun logout(
            @Field("uid") uidUser: String
    ): Observable<Auth.MessageResponse>


    @GET("goldar")
    fun goldar(): Observable<Auth.ListItemResponse>

    @GET("jen-kel")
    fun jenkel(): Observable<Auth.ListItemResponse>

    @GET("cek-no-hp")
    fun cekNoHp(
            @Query("noHp") noHp: String
    ): Observable<Auth.CekHp>

    @GET("sub-district")
    fun kecamatan(): Observable<Auth.ListKecamatanResponse>

    @GET("village-area/{id}")
    fun kelurahan(@Path("id") idKecamatan: String = "%"): Observable<Auth.ListKecamatanResponse>

    @FormUrlEncoded
    @POST("register")
    fun register(
            @Field("sub-district-id")idKecamatan:String,
            @Field("village-id")idKelurahan:String,
            @Field("nik")nik:String,
            @Field("name") nama: String,
            @Field("email") email: String="",
            @Field("password") pass: String,
            /*@Field("bpjs") bpjs: String,*/
            /*@Field("ktp") ktp: String,*/
            /*@Field("tglLahir") tglLahir: String,
            @Field("jenkel") kelamin: String,
            @Field("goldar") goldar: String,*/
            @Field("phone") hp: String,
            @Field("address") alamat: String,
            @Field("age") tglLahir: String,
            @Field("gender") kelamin: String
            /*@Field("idKecamatan") idKecamatan: String,
            @Field("idKelurahan") idKelurahan: String*/
    ): Observable<Auth.MessageResponse>

    @FormUrlEncoded
    @POST("auth/tambah-foto")
    fun tambahFoto(
            @Field("uidUser") uidUser: String,
            @Field("fotoKtp") fotoKtp: String,
            @Field("fotoUser") fotoUser: String
    ): Observable<Auth.MessageResponse>

    @Multipart
    @POST("register-verifying-selfie-ktp")
    fun uploadFotoSelfieKtp(
        @Part("uid")uid:RequestBody,
        @Part image1:MultipartBody.Part
    ):Observable<Auth.MessageResponse>

    @Multipart
    @POST("register-verifying-selfie")
    fun uploadFotoSelfie(
        @Part("uid")uid:RequestBody,
        @Part image1:MultipartBody.Part
    ):Observable<Auth.MessageResponse>

    @FormUrlEncoded
    @POST("auth/kirim-verifikasi")
    fun kirimVerifikasi(
            @Field("uidUser") uidUser: String
    ): Observable<Auth.MessageResponse>

    @FormUrlEncoded
    @POST("auth/verifikasi")
    fun verifikasi(
            @Field("uidUser") uidUser: String,
            @Field("kodeVerifikasi") kode: String
    ): Observable<Auth.MessageResponse>

    companion object Factory {
        fun create(): AuthService = RetrofitBuilder.create(AuthService::class.java)
    }
}
