package pt2.lunata.pscdeliserdang.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_list_layanan.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.textColor
import org.jetbrains.anko.toast
import pt2.lunata.pscdeliserdang.BaseActivity
import pt2.lunata.pscdeliserdang.afterTextChanged
import pt2.lunata.pscdeliserdang.R
import pt2.lunata.pscdeliserdang.api.LayananService
import pt2.lunata.pscdeliserdang.getColor

class ListLayananActivity : BaseActivity() {

    val compositeDisposable = CompositeDisposable()

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_layanan)

        setSupportActionBar(toolbar)
        appBar.outlineProvider = null
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle.visibility = View.VISIBLE
        toolbarTitle.textColor = getColor(this, R.color.pscAccent)
        toolbarTitle.text = "List Rumah Sakit"
        toolbar.backgroundColor = getColor(this, android.R.color.transparent)
        appBar.backgroundColor = getColor(this, android.R.color.transparent)

        val layananDisposable = LayananService.create()
                .listLayanan()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            val data = result.data
                            val llm = LinearLayoutManager(this)
                            val listDecoration = DividerItemDecoration(this@ListLayananActivity, llm.orientation)

                            recyclerView.layoutManager = llm
                            recyclerView.addItemDecoration(listDecoration)

                            recyclerView.adapter = LayananAdapter(data)
                        },
                        { throwable ->
                            toast("Mohon Cek Koneksi Anda")
                        }
                )
        compositeDisposable.add(layananDisposable)

        inputSearch.afterTextChanged { _ ->
            val pattern = inputSearch.text.toString().toLowerCase().trim()
            recyclerView.adapter?.let { _ ->
                (recyclerView.adapter as LayananAdapter).customFilter { fullList ->
                    fullList.filter {
                        if (pattern.isNotEmpty()) {
                            it.nama.toLowerCase().contains(pattern)
                        } else true
                    }
                }
            }
        }

    }
}
