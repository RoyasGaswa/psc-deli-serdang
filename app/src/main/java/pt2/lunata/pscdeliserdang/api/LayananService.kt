package pt2.lunata.pscdeliserdang.api

import io.reactivex.Observable
import pt2.lunata.pscdeliserdang.api.model.Layanan
import retrofit2.http.*

interface LayananService {
    @GET("layanan")
    fun listLayanan(): Observable<Layanan.LayananResponse>


    companion object factory {
        fun create(): LayananService = RetrofitBuilder.create(LayananService::class.java)
    }
}