package pt2.lunata.pscdeliserdang.api

import io.reactivex.Observable
import pt2.lunata.pscdeliserdang.api.model.Auth
import pt2.lunata.pscdeliserdang.api.model.Profil
import retrofit2.http.*

interface ProfilService {
    @GET("profil/detail")
    fun detail(
            @Query("uidUser") uidUser: String
    ): Observable<Profil.DetailRespon>

    @FormUrlEncoded
    @POST("user-data")
    fun userData(
        @Header("Authorization")token:String,
        @Field("uid")uid:String
    ):Observable<Profil.DetailResponUser>

    @FormUrlEncoded
    @POST("profil/edit")
    fun edit(
            @Field("uidUser") uidUser: String,
            @Field("email") email: String,
            @Field("bpjs") bpjs: String,
            @Field("hp") hp: String,
            @Field("alamat") alamat: String,
            @Field("idKecamatan") idKecamatan: String,
            @Field("idKelurahan") idKelurahan: String
    ): Observable<Auth.MessageResponse>

    @FormUrlEncoded
    @POST("profil/ganti-password")
    fun gantiPassword(
            @Field("uidUser") uidUser: String,
            @Field("passLama") passLama: String,
            @Field("passBaru") passBaru: String
    ): Observable<Auth.MessageResponse>

    @GET("status-verifikasi-user")
    fun statusVerifikasiUser(
            @Query("uidUser") uidUser: String
    ): Observable<Profil.StatusVerifikasiResponse>

    companion object Factory {
        fun create(): ProfilService = RetrofitBuilder.create(ProfilService::class.java)
    }
}
