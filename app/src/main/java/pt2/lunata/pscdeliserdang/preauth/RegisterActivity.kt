package pt2.lunata.pscdeliserdang.preauth

import android.app.DatePickerDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.View
import android.webkit.WebChromeClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat
import khronos.Dates
import khronos.Dates.today
import khronos.toDate
import khronos.year
import kotlinx.android.synthetic.main.activity_adukan_diri_sendiri.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.btnRegisterLanjut
import kotlinx.android.synthetic.main.activity_register.inputKecamatan
import kotlinx.android.synthetic.main.activity_register.inputKelurahan
import kotlinx.android.synthetic.main.activity_register.inputPassword
import kotlinx.android.synthetic.main.register_step.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.*
import pt2.lunata.pscdeliserdang.App
import pt2.lunata.pscdeliserdang.BaseActivity
import pt2.lunata.pscdeliserdang.R
import pt2.lunata.pscdeliserdang.api.AuthService
import pt2.lunata.pscdeliserdang.api.model.Auth
import pt2.lunata.pscdeliserdang.helper.AgeCalculator
import pt2.lunata.pscdeliserdang.toStringIndo

class RegisterActivity : BaseActivity() {
    companion object {
        const val CURRENT_BROADCAST = App.BC_REGISTER
    }

    val dataRegisterSharedPref = DataRegisterSharedPref()
    val TAG = this::class.java.simpleName
    val tempData = DataRegisterSharedPref()
    val compositeDisposable = CompositeDisposable()

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.action
            if (action.equals(CURRENT_BROADCAST)) {
                finish()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
        compositeDisposable.dispose()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        registerReceiver(broadcastReceiver, IntentFilter(CURRENT_BROADCAST))

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        appBar.outlineProvider = null
        toolbar.backgroundColor = pt2.lunata.pscdeliserdang.getColor(
            this@RegisterActivity,
            R.color.pscPrimary
        )

        registerStepView.statusView.currentCount = 1
        awalNIK.text="1207"
        awalNIK.isEnabled=false
        awalNIK.inputText.isFocusable=false
        awalNIK.inputText.keyListener=null
        inputPassword.layoutInputText.isPasswordVisibilityToggleEnabled = true
        val termString = textTerms.text.toString()
        val span = SpannableString(termString).apply { setSpan(UnderlineSpan(), 0, termString.length, 0) }
        textTerms.text = span

        textTerms.setOnClickListener { _ ->
            alert {
                title = "Syarat dan Ketentuan"

                val url = "https://dinkesmedan.lunata.co.id/psc/viewterms.php"

                customView {
                    verticalLayout {
                        lparams(matchParent, matchParent)

                        webView {
                        }.apply {
                            webChromeClient = WebChromeClient()
                            settings.javaScriptEnabled = true
                            loadUrl(url)
                        }.lparams(matchParent, dip(600))
                    }
                }

                positiveButton("Buka di Browser") {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                    startActivity(browserIntent)
                }

                negativeButton("Tutup") { }
            }.show()
        }

        var lahirTgl = 1
        var lahirBln = 0 // bln nya pk array
        var lahirThn = 1980

        val onClickListenerTglLahir = View.OnClickListener {
            val dpd = DatePickerDialog(
                    this@RegisterActivity,
                    R.style.RedDialog, DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                inputTglLahir.inputText.setText(Dates.of(year, month + 1, dayOfMonth).toStringIndo("dd MMMM yyyy"))
                inputTglLahir.value = "$year-${month + 1}-$dayOfMonth"
                lahirTgl = dayOfMonth
                lahirBln = month
                lahirThn = year
            }, lahirThn, lahirBln, lahirTgl
            )

            dpd.datePicker.minDate = 90.year.ago.time
            dpd.datePicker.maxDate = today.time
            dpd.show()
        }

        inputTglLahir.inputText.setOnClickListener(onClickListenerTglLahir)
        inputTglLahir.icon.setOnClickListener(onClickListenerTglLahir)


        val onClickListenerJenkel = View.OnClickListener {
            inputJenKel.inputText.isEnabled = false
            inputJenKel.icon.isEnabled = false
            val dataJenkel= ArrayList<Auth.Item>()
            dataJenkel.add(Auth.Item("Perempuan","F"))
            dataJenkel.add(Auth.Item("Laki-Laki","M"))
            selector("Jenis Kelamin",
                dataJenkel.map { it.nama }
            ) { _, pos ->
                inputJenKel.inputText.setText(dataJenkel[pos].nama)
                inputJenKel.value = dataJenkel[pos].uid
            }
            inputJenKel.inputText.isEnabled = true
            inputJenKel.icon.isEnabled = true
            /*val jenkelDisposable =
                    AuthService.create()
                            .jenkel()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    { result ->
                                        val dataJenkel = result.data

                                    },
                                    { throwable ->
                                        inputJenKel.inputText.isEnabled = true
                                        inputJenKel.icon.isEnabled = true
                                        Log.e(TAG, throwable.message)
                                        longToast("Mohon Cek Koneksi Anda")
                                    }
                            )

            compositeDisposable.add(jenkelDisposable)*/
        }

        inputJenKel.inputText.setOnClickListener(onClickListenerJenkel)
        inputJenKel.icon.setOnClickListener(onClickListenerJenkel)


        /*val onClickListenerGoldar = View.OnClickListener {
            inputGolDar.inputText.isEnabled = false
            inputGolDar.icon.isEnabled = false
            val goldarDisposable =
                    AuthService.create()
                            .goldar()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    { result ->
                                        val dataGoldar = result.data
                                        selector("Golongan Darah",
                                                dataGoldar.map { it.nama }
                                        ) { _, pos ->
                                            inputGolDar.inputText.setText(dataGoldar[pos].nama)
                                            inputGolDar.value = dataGoldar[pos].uid
                                        }
                                        inputGolDar.inputText.isEnabled = true
                                        inputGolDar.icon.isEnabled = true
                                    },
                                    { throwable ->
                                        inputGolDar.inputText.isEnabled = true
                                        inputGolDar.icon.isEnabled = true
                                        Log.e(TAG, throwable.message)
                                        longToast("Mohon Cek Koneksi Anda")
                                    }
                            )

            compositeDisposable.add(goldarDisposable)
        }

        inputGolDar.inputText.setOnClickListener(onClickListenerGoldar)
        inputGolDar.icon.setOnClickListener(onClickListenerGoldar)*/


        val onClickListenerKecamatan = View.OnClickListener { _ ->
            inputKecamatan.inputText.isEnabled = false
            inputKecamatan.icon.isEnabled = false
            val kecamatanDisposable =
                    AuthService.create()
                            .kecamatan()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    { result ->
                                        val dataKecamatan =
                                                result.data.map { Auth.KecamatanSearchableItem(it.name, it.id) }.sortedBy { it.name }
                                        //val data=resources.getStringArray(R.array.listKecamatan) as List<String>
                                        SimpleSearchDialogCompat(
                                                this@RegisterActivity, "Kecamatan", "Cari..."
                                                , null, ArrayList(dataKecamatan)
                                        ) { dialog, item, _ ->
                                            inputKecamatan.text = item.name
                                            inputKecamatan.value = item.id
                                            inputKelurahan.text=""
                                            inputKelurahan.value=null
                                            dialog.hide()
                                        }.show()

                                        inputKecamatan.inputText.isEnabled = true
                                        inputKecamatan.icon.isEnabled = true
                                    },
                                    { throwable ->
                                        inputKecamatan.inputText.isEnabled = true
                                        inputKecamatan.icon.isEnabled = true
                                        Log.e(TAG, throwable.message)
                                        longToast("Mohon Cek Koneksi Anda")
                                    }
                            )

            compositeDisposable.add(kecamatanDisposable)
        }

        inputKecamatan.inputText.setOnClickListener(onClickListenerKecamatan)
        inputKecamatan.icon.setOnClickListener(onClickListenerKecamatan)


        val onClickListenerKelurahan = View.OnClickListener { _ ->
            if (inputKecamatan.value != null) {
                inputKelurahan.inputText.isEnabled = false
                inputKelurahan.icon.isEnabled = false
                val kelurahanDisposable =
                        AuthService.create()
                                .kelurahan(inputKecamatan.value!!)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        { result ->
                                            val dataKelurahan =
                                                    result.data.map { Auth.KecamatanSearchableItem(it.name, it.id) }
                                                            .sortedBy { it.name }

                                            SimpleSearchDialogCompat(
                                                    this@RegisterActivity, "Kelurahan", "Cari..."
                                                    , null, ArrayList(dataKelurahan)
                                            ) { dialog, item, _ ->
                                                inputKelurahan.text = item.name
                                                inputKelurahan.value = item.id
                                                dialog.hide()
                                            }.show()

                                            inputKelurahan.inputText.isEnabled = true
                                            inputKelurahan.icon.isEnabled = true
                                        },
                                        { throwable ->
                                            inputKelurahan.inputText.isEnabled = true
                                            inputKelurahan.icon.isEnabled = true
                                            Log.e(TAG, throwable.message)
                                            longToast("Mohon Cek Koneksi Anda")
                                        }
                                )

                compositeDisposable.add(kelurahanDisposable)
            } else {
                toast("Mohon pilih kecamatan terlebih dahulu")
            }
        }

        inputKelurahan.inputText.setOnClickListener(onClickListenerKelurahan)
        inputKelurahan.icon.setOnClickListener(onClickListenerKelurahan)


        /*inputNoHp.inputText.onFocusChange { _, hasFocus ->
            if (!hasFocus) {
                val cekHpDisposable = AuthService.create()
                        .cekNoHp(inputNoHp.text)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { result ->
                                    if (result.jumlah > 0) {
                                        inputNoHp.inputText.error = "Nomor Hp sudah terdaftar"
                                        alert {
                                            message = "Nomor HP sudah terdaftar. Mohon input nomor lain.\nJika anda ingin mem-verifikasi email, silahkan login."
                                            negativeButton("Tutup") {}
                                        }.show()
                                    } else {
                                        inputNoHp.inputText.error = null
                                    }
                                },
                                {
                                }
                        )
                compositeDisposable.add(cekHpDisposable)
            }
        }*/

        btnRegisterLanjut.setOnClickListener {
            if (switchTerms.isChecked) {
                if (validateForm()) {
                    val nik= awalNIK.text+inputNIK.text
                    val nama = inputNama.text
                    val email = inputEmail.text
                    val pass = inputPassword.text
                    /*val bpjs = inputNoBpjs.text*/
                    //val ktp = inputNoKtp.text
                    val tglLahir = inputTglLahir.value!!
                    userPref.tglLahir=tglLahir
                    val umur=AgeCalculator.calculateAge(tglLahir.toDate("yyyy-MM-dd")).years
                    val kelamin = inputJenKel.value!!
                    //val goldar = inputGolDar.value!!
                    val noHp = inputNoHp.text
                    val alamat = inputAlamat.text
                    val idKecamatan = inputKecamatan.value!!
                    val idKelurahan = inputKelurahan.value!!

                    if (nik.length<16 || nik.length>16){
                        toast("NIK Tidak Valid")
                        inputNIK.inputText.setError("Wajib 16 Anggka")
                    }else if(awalNIK.text!="1207"){
                        toast("NIK Tidak Valid")
                        awalNIK.inputText.setError("NIK Tidak valid")
                    } else if (inputNoHp.text.length<11 || inputNoHp.text.length>12){
                        toast("Nomor Hp Tidak Valid")
                        inputNoHp.inputText.setError("Wajib 11-12 Angka")
                    }else if (inputPassword.text.length<8){
                        toast("Password minimal 8 karakter")
                    }else if (inputKecamatan.value==null){
                        toast("Pilih Kecamatan")
                    } else{
                        tempData.clear()
                        tempData.apply {
                            this.nik=nik
                            this.nama = nama
                            this.email = email
                            this.pass = pass
                            //this.bpjs = bpjs
                            //this.ktp = ktp
                            this.tglLahir = tglLahir
                            this.kelamin = kelamin
                            //this.goldar = goldar
                            this.noHp = noHp
                            this.alamat = alamat
                            this.idKecamatan = idKecamatan
                            this.idKelurahan = idKelurahan
                        }
                        val alertUpload = indeterminateProgressDialog("Sedang mengirim data") {
                            setCancelable(false)
                        }
                        alertUpload.show()
                        val registerDisposable =
                            AuthService.create()
                                .register(
                                    idKecamatan,
                                    idKelurahan,
                                    nik,
                                    nama,
                                    email,
                                    pass,
                                    noHp,
                                    alamat,
                                    umur.toString(),
                                    kelamin
                                )
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                    { result ->
                                        val message = result.message
                                        if (message=="Success registtred data !"){
                                            val loginDisposable = AuthService.create()
                                                .login(inputNoHp.text,inputPassword.text,App.DEVICE_TOKEN)
                                                .subscribeOn(Schedulers.io())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(
                                                    { result ->
                                                        val data = result.data
                                                        if (result.message=="Success authenticated Data !"){
                                                            alertUpload.hide()
                                                            tempData.apply {
                                                                this.uid=data.uid
                                                            }
                                                            startActivity<FotoKtpActivity>()
                                                            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                                                            finish()
                                                        }
                                                    },
                                                    {
                                                        toast("Ada masalah. Mohon cek koneksi ataupun password anda")
                                                    }
                                                )

                                            compositeDisposable.add(loginDisposable)
                                        }else if(message=="NIK already exist!"){
                                            toast("NIK sudah pernah digunakan!")
                                            inputNIK.inputText.setError("NIK Sudah Ada!")
                                            btnRegisterLanjut.isEnabled = true
                                            alertUpload.hide()
                                        }else if (message=="Phone Number already exist!"){
                                            toast("No hp sudah pernah digunakan!")
                                            inputNoHp.inputText.setError("No hp sudah pernah digunakan!")
                                            btnRegisterLanjut.isEnabled = true
                                            alertUpload.hide()
                                        }

                                    },
                                    { t ->
                                        btnRegisterLanjut.isEnabled = true
                                        alertUpload.hide()

                                        Log.e(TAG, t.message)
                                        longToast("Mohon cek koneksi anda")
                                    }
                                )
                        compositeDisposable.add(registerDisposable)
                    }
                } else {
                    btnRegisterLanjut.isEnabled = true
                    longToast("Harap isi semua data")
                }
            } else {
                btnRegisterLanjut.isEnabled = true
                longToast("Harap setujui syarat dan ketentuan")
            }
        }
    }

    private fun validateForm() =
            /*inputNama.isValid() && inputEmail.isValid()
                    && inputPassword.isValid() && inputNoKtp.isValid()
                    && inputTglLahir.isValid() && inputJenKel.isValid()
                    && inputGolDar.isValid() && inputNoHp.isValid()
                    && inputAlamat.isValid() && inputNoBpjs.isValid()
                    && inputKecamatan.isValid() && inputKelurahan.isValid()*/
        inputNama.isValid() && inputNIK.isValid()
                && inputPassword.isValid() && inputNoHp.isValid()
                && inputAlamat.isValid() && inputKecamatan.isValid()
}

