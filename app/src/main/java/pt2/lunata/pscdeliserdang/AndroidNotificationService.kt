package pt2.lunata.pscdeliserdang

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.RemoteMessage
import com.google.firebase.messaging.FirebaseMessagingService
import pt2.lunata.pscdeliserdang.preauth.SplashActivity
import java.util.*

class AndroidNotificationService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        remoteMessage?.notification?.let {
            val mainIntent = Intent(this, SplashActivity::class.java)
            val notificationId = ((Date().time / 1000L) % Integer.MAX_VALUE).toInt()
            val pendingIntent = PendingIntent.getActivity(this, 0, mainIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            val notificationSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

            val notification = NotificationCompat.Builder(this, "psc_user").apply {
                setContentTitle(remoteMessage.notification!!.title)
                setContentText(remoteMessage.notification!!.body)
                setContentIntent(pendingIntent)

                setSound(notificationSoundUri)
                setSmallIcon(R.drawable.ic_ico_logo_ambulans_deliserdang)

                setVibrate(longArrayOf(300, 1000, 500, 1000))

                color = getColor(applicationContext, R.color.pscPrimary)
            }

            val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel("psc_user", "Default channel", NotificationManager.IMPORTANCE_DEFAULT)
                notification.setChannelId("psc_user")
                manager.createNotificationChannel(channel)
            }
            val notificationBuild= notification.build()
            manager.notify(notificationId, notificationBuild)
        }
    }
}