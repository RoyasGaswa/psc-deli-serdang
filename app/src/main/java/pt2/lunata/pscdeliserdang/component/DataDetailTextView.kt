package pt2.lunata.pscdeliserdang.component

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import android.widget.TextView
import org.jetbrains.anko.textColor
import pt2.lunata.pscdeliserdang.R

@SuppressLint("SetTextI18n")
class DataDetailTextView(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

    var text: String
        get() = textView.text.toString()
        set(value) {
            textView.text = value
        }

    val labelView: TextView
    val textView: TextView
    val layoutContainer: LinearLayout

    init {
        inflate(context, R.layout.component_data_detail_text_view, this)

        labelView = findViewById(R.id.label)
        textView = findViewById(R.id.text)
        layoutContainer = findViewById(R.id.layoutContainer)

        @SuppressLint("CustomViewStyleable")
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.ComponentDataDetailTextView)
        labelView.text = attributes.getString(R.styleable.ComponentDataDetailTextView_label)
        textView.text = attributes.getString(R.styleable.ComponentDataDetailTextView_text)
        textView.textColor = attributes.getColor(R.styleable.ComponentDataDetailTextView_android_textColor, 0)
        labelView.textColor = attributes.getColor(R.styleable.ComponentDataDetailTextView_android_textColor, 0)

        attributes.recycle()
    }
}