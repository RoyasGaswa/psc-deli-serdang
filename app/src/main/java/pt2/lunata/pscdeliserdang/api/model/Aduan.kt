package pt2.lunata.pscdeliserdang.api.model

import android.os.Parcelable
import ir.mirrajabi.searchdialog.core.Searchable
import kotlinx.android.parcel.Parcelize

object Aduan {
    data class ListItemResponse(val data: List<Item>, val message: String)
    data class Item(
            val nama: String,
            val id: String
    )

    data class ListHubkelResponse(val data : List<Hubkel>, val message: String)
    data class Hubkel(
            val nama:String,
            val uid: String,
            val urutan: String
    )

    data class SearchableItem(val nama: String, val uid: String) : Searchable {
        override fun getTitle(): String = nama
    }

    data class HubkelSearchableItem(val nama: String, val uid: String, val urutan: String) : Searchable {
        override fun getTitle(): String = nama
    }

    data class CekAduan(val jumlah: Int, val message: String)

    data class MessageResponse(val message: String)

    data class HistoryResponse(val data: List<History>, val message: String)
    @Parcelize
    data class History(
            val diseases_name: String,
            val report_type: String,
            val created_at: String,
            val people_uid: String,
            val id: Int,
            val address: String,
            val reporter_name:String,
            val people_appends:dataPasien
    ):Parcelable
    @Parcelize
    data class dataPasien(
        val age:String,
        val gender:String
    ):Parcelable

    data class DetailResponse(val data: Detail, val message: String)
    data class Detail(
            val tanggalAduan: String,
            val jenisPenyakit: String,
            val tipeAduan: String,
            val keterangan: String,
            val nama: String,
            val umur: String,
            val jenisKelamin: String,
            val hubkel: String,
            val alamatKejadian: String,
            val koorKejadian: String,
            val alamatRumahSakit: String?,
            val koorRumahSakit: String?,
            val foto: String,
            val idPengaduanStatus: Int,
            val statusPengaduan: String
    )
}

