package pt2.lunata.pscdeliserdang.preauth

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.json.JSONObject
import pt2.lunata.pscdeliserdang.App
import pt2.lunata.pscdeliserdang.BaseActivity
import pt2.lunata.pscdeliserdang.R
import pt2.lunata.pscdeliserdang.api.AuthService
import pt2.lunata.pscdeliserdang.api.ProfilService
import pt2.lunata.pscdeliserdang.main.DashboardActivity
import retrofit2.adapter.rxjava2.HttpException

class LoginActivity : BaseActivity() {

    private val compositeDisposable = CompositeDisposable()
    val dataRegisterSharedPref = DataRegisterSharedPref() // kalau belum verifikasi email

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        inputPassword.layoutInputText.isPasswordVisibilityToggleEnabled = true

        btnLogin.setOnClickListener { view ->
            view.isEnabled = false
            Log.d(TAG,App.DEVICE_TOKEN.toString())
            if (inputHp.isValid() && inputPassword.isValid()) {
                val loginDisposable = AuthService.create()
                        .login(inputHp.text, inputPassword.text,App.DEVICE_TOKEN)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { result ->
                                    val data = result.data
                                    userPref.token=data.token
                                    val userDataDisposable=ProfilService.create()
                                        .userData("Bearer "+data.token,data.uid)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(
                                            {hasil->
                                                val user=hasil.data
                                                // jangan lupa ganti jugak di splash activity
                                                userPref.uid = data.uid
                                                userPref.nama = user.name
                                                userPref.alamat=user.address
                                                userPref.hp = user.phone
                                                userPref.fotoUrl = user.selfie
                                                userPref.kecamatan=user.subdistrict_area
                                                userPref.kelurahan=user.village_area
                                                userPref.idStatus = user.approved
                                                userPref.deleted_at=user.deleted_at
                                                userPref.uidKelamin=user.gender
                                                userPref.umur=user.age
                                                userPref.idKecamatan=user.subdistrict_id
                                                if (user.selfie==null && user.selfieKtp == null) {
                                                    dataRegisterSharedPref.uid = data.uid
                                                    userPref.uid=null
                                                    startActivity<FotoKtpActivity>()
                                                    finish()
                                                }else if (user.rejection!=null){
                                                    userPref.uid=null
                                                    toast(user.rejection)
                                                    view.isEnabled=true
                                                }else if (user.approved==null){
                                                    userPref.uid=null
                                                    Toast.makeText(this,"Akun Anda Belum diverifikasi oleh Admin..",Toast.LENGTH_SHORT).show()
                                                    view.isEnabled=true
                                                }else {
                                                    startActivity<DashboardActivity>()
                                                    finish()
                                                }
                                        },
                                            {
                                                toast("Ada masalah. Mohon cek koneksi ataupun password anda")
                                            }
                                        )
                                    compositeDisposable.add(userDataDisposable)

                                    /*if (data.idStatus == 1) {
                                        dataRegisterSharedPref.uid = data.uid
                                        startActivity<VerifikasiActivity>()
                                        finish()
                                    } else {
                                        startActivity<DashboardActivity>()
                                        finish()
                                    }*/
                                },
                                {
                                    val code=(it as HttpException).code().toString()
                                    val body=(it as HttpException).response()?.errorBody()?.string()
                                    val message=JSONObject(body)
                                    if (code=="401"){
                                        toast(message["message"].toString())
                                    }else{
                                        toast("Ada masalah. Mohon cek koneksi ataupun password anda")
                                    }
                                    view.isEnabled = true
                                }
                        )

                compositeDisposable.add(loginDisposable)
            } else {
                toast("Lengkapi Form terlebih dahulu")
                view.isEnabled = true
            }
        }
    }
}
