package pt2.lunata.pscdeliserdang.main

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_webview.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.textColor
import pt2.lunata.pscdeliserdang.BaseActivity
import pt2.lunata.pscdeliserdang.R
import pt2.lunata.pscdeliserdang.getColor

class WebviewActivity : BaseActivity() {

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)

        val title = intent.getStringExtra("activity-title")
        val url = intent.getStringExtra("url")

        setSupportActionBar(toolbar)
        appBar.outlineProvider = null
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbarTitle.visibility = View.VISIBLE
        toolbarTitle.textColor = getColor(this@WebviewActivity, R.color.pscAccent)
        toolbarTitle.text = title

        swipeRefreshLayout.isRefreshing = true

        webView.apply {
            webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    swipeRefreshLayout.isRefreshing = false
                    swipeRefreshLayout.isEnabled = false
                }
            }
            settings.javaScriptEnabled = true
            loadUrl(url)
        }
    }

}
