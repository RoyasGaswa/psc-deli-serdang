package pt2.lunata.pscdeliserdang.preauth

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_landing.*
import org.jetbrains.anko.startActivity
import pt2.lunata.pscdeliserdang.App
import pt2.lunata.pscdeliserdang.BaseActivity
import pt2.lunata.pscdeliserdang.R

class LandingActivity : BaseActivity() {
    companion object {
        const val CURRENT_BROADCAST = App.BC_LANDING
    }

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.action
            if (action.equals(CURRENT_BROADCAST)) {
                finish()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing)

        registerReceiver(broadcastReceiver, IntentFilter(CURRENT_BROADCAST))

        supportActionBar?.hide()

        btnRegister.setOnClickListener {
            startActivity<RegisterActivity>()
        }

        btnLogin.setOnClickListener { _ ->
            startActivity<LoginActivity>()
        }
    }
}
