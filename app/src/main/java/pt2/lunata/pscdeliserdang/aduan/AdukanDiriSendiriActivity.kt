package pt2.lunata.pscdeliserdang.aduan

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.LocationServices
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat
import kotlinx.android.synthetic.main.activity_adukan_diri_sendiri.*
import kotlinx.android.synthetic.main.toolbar.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.jetbrains.anko.*
import pt2.lunata.pscdeliserdang.*
import pt2.lunata.pscdeliserdang.api.AduanService
import pt2.lunata.pscdeliserdang.api.AuthService
import pt2.lunata.pscdeliserdang.api.model.Aduan
import pt2.lunata.pscdeliserdang.api.model.Auth
import java.io.File
import java.io.FileOutputStream
import java.util.*


class AdukanDiriSendiriActivity : BaseActivity() {
    companion object {
        const val RC_CAMERA_RESULT = 13
        const val RC_MAP_RESULT = 14

        const val RP_FINE_LOCATION = 20
    }

    private val compositeDisposable = CompositeDisposable()
    lateinit var base64Foto: String
    lateinit var fotoPathAduan:String
    var long=""
    var lat=""
    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adukan_diri_sendiri)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbarTitle.text = "Form Pengaduan"
        toolbarTitle.textColor = getColor(this@AdukanDiriSendiriActivity, R.color.pscAccent)
        toolbarTitle.visibility = View.VISIBLE

        appBar.outlineProvider = null
        toolbar.backgroundColor = getColor(
            this,
            R.color.pscPrimary
        )

        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                RP_FINE_LOCATION
            )
            return
        } else {
            setLocation()
        }

        val onClickListenerKeadaan = View.OnClickListener {
            inputKeadaan.inputText.isEnabled = false
            inputKeadaan.icon.isEnabled = false
            val keadaanDisposable = AduanService.create()
                .listPenyakit()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->
                        val dataPenyakit = result.data.map { Aduan.SearchableItem(it.nama, it.id) }

                        SimpleSearchDialogCompat(
                            this@AdukanDiriSendiriActivity,
                            "Nama Keadaan Darurat",
                            "Cari...",
                            null,
                            ArrayList(dataPenyakit)
                        ) { dialog, item, _ ->
                            inputKeadaan.text = item.nama
                            inputKeadaan.value = item.uid
                            dialog.hide()
                        }.show()
                        inputKeadaan.inputText.isEnabled = true
                        inputKeadaan.icon.isEnabled = true
                    },
                    { throwable ->
                        inputKeadaan.inputText.isEnabled = true
                        inputKeadaan.icon.isEnabled = true
                        Log.e(TAG, throwable.message)
                        longToast("Mohon Cek Koneksi Anda")
                    }
                )
            compositeDisposable.add(keadaanDisposable)
        }

        inputKeadaan.inputText.setOnClickListener(onClickListenerKeadaan)
        inputKeadaan.icon.setOnClickListener(onClickListenerKeadaan)

        val onClickListenerKecamatan = View.OnClickListener { _ ->
            inputKecamatan.inputText.isEnabled = false
            inputKecamatan.icon.isEnabled = false
            val kecamatanDisposable =
                AuthService.create()
                    .kecamatan()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { result ->
                            val dataKecamatan =
                                result.data.map { Auth.KecamatanSearchableItem(it.name, it.id) }
                                    .sortedBy { it.name }
                            //val data=resources.getStringArray(R.array.listKecamatan) as List<String>
                            SimpleSearchDialogCompat(
                                this@AdukanDiriSendiriActivity,
                                "Kecamatan",
                                "Cari...",
                                null,
                                ArrayList(dataKecamatan)
                            ) { dialog, item, _ ->
                                inputKecamatan.text = item.name
                                inputKecamatan.value = item.id
                                inputKelurahan.text=""
                                inputKelurahan.value=null
                                dialog.hide()
                            }.show()

                            inputKecamatan.inputText.isEnabled = true
                            inputKecamatan.icon.isEnabled = true
                        },
                        { throwable ->
                            inputKecamatan.inputText.isEnabled = true
                            inputKecamatan.icon.isEnabled = true
                            Log.e(TAG, throwable.message)
                            longToast("Mohon Cek Koneksi Anda")
                        }
                    )

            compositeDisposable.add(kecamatanDisposable)
        }

        inputKecamatan.inputText.setOnClickListener(onClickListenerKecamatan)
        inputKecamatan.icon.setOnClickListener(onClickListenerKecamatan)


        val onClickListenerKelurahan = View.OnClickListener { _ ->
            if (inputKecamatan.value != null) {
                inputKelurahan.inputText.isEnabled = false
                inputKelurahan.icon.isEnabled = false
                val kelurahanDisposable =
                    AuthService.create()
                        .kelurahan(inputKecamatan.value!!)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                            { result ->
                                val dataKelurahan =
                                    result.data.map { Auth.KecamatanSearchableItem(it.name, it.id) }
                                        .sortedBy { it.name }

                                SimpleSearchDialogCompat(
                                    this@AdukanDiriSendiriActivity,
                                    "Kelurahan",
                                    "Cari...",
                                    null,
                                    ArrayList(dataKelurahan)
                                ) { dialog, item, _ ->
                                    inputKelurahan.text = item.name
                                    inputKelurahan.value = item.id
                                    dialog.hide()
                                }.show()

                                inputKelurahan.inputText.isEnabled = true
                                inputKelurahan.icon.isEnabled = true
                            },
                            { throwable ->
                                inputKelurahan.inputText.isEnabled = true
                                inputKelurahan.icon.isEnabled = true
                                Log.e(TAG, throwable.message)
                                longToast("Mohon Cek Koneksi Anda")
                            }
                        )

                compositeDisposable.add(kelurahanDisposable)
            } else {
                toast("Mohon pilih kecamatan terlebih dahulu")
            }
        }

        inputKelurahan.inputText.setOnClickListener(onClickListenerKelurahan)
        inputKelurahan.icon.setOnClickListener(onClickListenerKelurahan)

        inputLokasi.icon.setImageDrawable(
            getDrawable(
                this@AdukanDiriSendiriActivity,
                R.drawable.ic_location_on_black_24dp
            )
        )

        setLocation()

        inputLokasi.inputText.setOnClickListener {
            val mapIntent = Intent(this@AdukanDiriSendiriActivity, OpenMapsActivity::class.java)
            startActivityForResult(mapIntent, RC_MAP_RESULT)
        }

        btnAddFoto.setOnClickListener {
            val cameraIntent = Intent(this@AdukanDiriSendiriActivity, CameraActivity::class.java)
            cameraIntent.putExtra("confirmation", false)
            cameraIntent.putExtra("gallery-picker", false)
            startActivityForResult(cameraIntent, RC_CAMERA_RESULT)
        }

        btnKirim.setOnClickListener { view ->
            view.isEnabled = false
            if (inputKeadaan.isValid() && inputKeterangan.isValid() && inputLokasi.isValid() && ::fotoPathAduan.isInitialized) {
                val nama = userPref.nama!!
                val koordinat = inputLokasi.value!!
                //val umur = calculateAge(userPref.tglLahir!!.toDate("yyyy-MM-dd")).years
                val uidUser = userPref.uid!!
                //val uidJenkel = userPref.uidKelamin!!

                val file = File(fotoPathAduan)
                val imageAduan= RequestBody.create("image/*".toMediaTypeOrNull(), file)
                val filePart= MultipartBody.Part.createFormData(
                    "prove_images1",
                    file.name,
                    imageAduan
                )

                val stringMediaType = "text/plain".toMediaTypeOrNull()
                val uid = userPref.uid?.toRequestBody(stringMediaType)
                val uidPenyakit = inputKeadaan.value!!.toRequestBody(stringMediaType)
                val keterangan = inputKeterangan.text.toRequestBody(stringMediaType)
                val alamat = inputLokasi.text.toRequestBody(stringMediaType)
                val reportType="1".toRequestBody(stringMediaType)
                val longtitude=long.toRequestBody(stringMediaType)
                val kelurahan=inputKelurahan.value!!.toRequestBody(stringMediaType)
                val latitude=lat.toRequestBody(stringMediaType)
                val aduanDisposable = AduanService.create()
                    .adukanDiriSendiri(
                        "Bearer " + userPref.token!!,
                        uid!!,
                        uidPenyakit,
                        reportType,
                        keterangan,
                        alamat,
                        kelurahan,
                        longtitude,
                        latitude,
                        filePart
                    )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        {
                            startActivity<SplashInfoPesananActivity>()
                            finish()
                        },
                        { throwable ->
                            view.isEnabled = true
                            Log.e(TAG, throwable.message)
                            longToast("Mohon Cek Koneksi Anda")
                        }
                    )

                compositeDisposable.add(aduanDisposable)
            } else {
                view.isEnabled = true
                toast("Lengkapi Form terlebih dahulu")
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun setLocation() {
        LocationServices.getFusedLocationProviderClient(this).lastLocation.addOnSuccessListener { location ->
            val geocoder = Geocoder(this@AdukanDiriSendiriActivity, Locale.getDefault())
            val address = geocoder.getFromLocation(location.latitude, location.longitude, 1)
            inputLokasi.text = address[0].getAddressLine(0)
            inputLokasi.value = "${location.latitude},${location.longitude}"
            lat=location.latitude.toString()
            long=location.longitude.toString()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RC_CAMERA_RESULT -> {
                if (resultCode == Activity.RESULT_OK) {
                    val alertUpload = indeterminateProgressDialog("Sedang memproses gambar") {
                        setCancelable(false)
                    }
                    alertUpload.show()

                    val fotoPath = data?.getStringExtra("foto-path")
                    val bitmapFactoryOptions = BitmapFactory.Options()
                    var bitmap = BitmapFactory.decodeFile(fotoPath, bitmapFactoryOptions)

                    if (bitmap.width > App.FOTO_MAX_WIDTH_OR_HEIGHT || bitmap.height > App.FOTO_MAX_WIDTH_OR_HEIGHT) {
                        bitmap = bitmap.scale(800)
                    }
                    val file_path = Environment.getExternalStorageDirectory().absolutePath +
                            "/PhysicsSketchpad"
                    val dir = File(file_path)
                    if (!dir.exists()) dir.mkdirs()
                    val fotoname=Calendar.getInstance().getTime().toString()
                    val file = File(dir, fotoname + ".png")
                    val fOut = FileOutputStream(file)

                    bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut)
                    fOut.flush()
                    fOut.close()
                    fotoPathAduan = file.absolutePath
                    layoutFoto.visibility = View.VISIBLE
                    imgFotoPreview.imageBitmap = bitmap

                    fabPreviewFoto.setOnClickListener {
                        alert {
                            customView {
                                linearLayout {
                                    lparams(matchParent, matchParent)
                                    zoomableImageView {
                                        backgroundColor = getColor(
                                            this@AdukanDiriSendiriActivity,
                                            R.color.material_light_black
                                        )
                                        imageBitmap = bitmap
                                    }.lparams(matchParent, matchParent)
                                }
                            }
                        }.show()
                    }

                    base64Foto = bitmapToBase64(bitmap)
                    alertUpload.hide()
                } else {
                    longToast("Ada masalah dengan foto")
                }
            }
            RC_MAP_RESULT -> {
                if (resultCode == Activity.RESULT_OK) {
                    inputLokasi.textNullable =
                        data?.extras?.getParcelable<Address>(App.RI_MAP_ORIGIN)?.getAddressLine(
                            0
                        )
                } else {
                    longToast("Ada masalah dengan data map")
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            RP_FINE_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setLocation()
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    longToast("Mohon izinkan pemakaian lokasi")
                    finish()
                }
            }
        }
    }
}
